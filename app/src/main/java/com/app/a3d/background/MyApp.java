package com.app.a3d.background;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.IntentFilter;
import android.media.AudioAttributes;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.text.TextUtils;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.BuildConfig;
import androidx.multidex.MultiDex;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferNetworkLossHandler;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.app.a3d.R;

public class MyApp extends Application {

    private static MyApp mInstance;
    private RequestQueue mRequestQueue;
    public static Context context;
    public static final String TAG = MyApp.class.getSimpleName();


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }


    public static synchronized MyApp getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public static Context getContext() {
        return context;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        if (!BuildConfig.DEBUG) {
//            Fabric.with(this, new Crashlytics());
        }
        context = getApplicationContext();
        mInstance = this;


        registerReceiver(
                TransferNetworkLossHandler.getInstance(getApplicationContext()), new IntentFilter(
                        ConnectivityManager.CONNECTIVITY_ACTION
                )
        );

        createNotificationChannels();

    }


    private void createNotificationChannels() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            NotificationChannel chatGroupHangUp = new NotificationChannel(
                    "Notification",
                    "Notification",
                    NotificationManager.IMPORTANCE_HIGH
            );
            notificationManager.createNotificationChannel(chatGroupHangUp);

            Uri sound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + this.getPackageName() + "/" + R.raw.driver_notification);  //Here is FILE_NAME is the name of file that you want to play
            AudioAttributes attributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .build();
            NotificationChannel channel_all = new NotificationChannel("Notification_Sound", "Notification_Sound", NotificationManager.IMPORTANCE_HIGH);
            channel_all.enableVibration(true);
            channel_all.setSound(sound, attributes); // This is IMPORTANT
            notificationManager.createNotificationChannel(channel_all);
        }

    }


}
