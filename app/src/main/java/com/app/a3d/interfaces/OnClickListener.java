package com.app.a3d.interfaces;

public interface OnClickListener {

    void onClick(int position);
}
