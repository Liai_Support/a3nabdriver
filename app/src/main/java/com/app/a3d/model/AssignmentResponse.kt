package com.app.a3d.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AssignmentResponse : Serializable {

    @SerializedName("error")
    var error: Boolean = true

    @SerializedName("message")
    var message: String = ""

    @SerializedName("data")
    var data: AssignmentData = AssignmentData()

}

class AssignmentData : Serializable {

    @SerializedName("completed")
    var completed: ArrayList<Assignment> = ArrayList()

    @SerializedName("current")
    var current: ArrayList<Assignment> = ArrayList()

    @SerializedName("upcoming")
    var upcoming: ArrayList<Assignment> = ArrayList()

}

class Assignment : Serializable {

    @SerializedName("id")
    var id: String = ""

  @SerializedName("assignDate")
    var assignDate: String? = ""

}
