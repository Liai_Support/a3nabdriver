package com.app.a3d.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CommonResponse : Serializable {

    @SerializedName("error")
    var error: Boolean = true

    @SerializedName("message")
    var message: String = ""


}



