package com.app.a3d.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class DashboardResponse : Serializable {

    @SerializedName("error")
    var error = true

    @SerializedName("message")
    var message = ""

    @SerializedName("data")
    var data = DashBoardDataData()

}

class DashBoardDataData : Serializable {
    @SerializedName("profile")
    var profile = DashboardData()

    @SerializedName("newCar")
    var newCar = NewCar()

    @SerializedName("currentAssignment")
    var currentAssignment: ArrayList<CurrentAssignment> = ArrayList()
}


class NewCar : Serializable {

    @SerializedName("car")
    var car: ArrayList<CarDetails> = ArrayList()

   @SerializedName("damage")
    var damage: ArrayList<Damages> = ArrayList()



}


class CarDetails : Serializable {

    @SerializedName("id")
    var id = ""

    @SerializedName("firstName")
    var firstName = ""

    @SerializedName("lastName")
    var lastName = ""

    @SerializedName("carID")
    var carID: String? = ""

    @SerializedName("ucarId")
    var ucarId: String? = ""

    @SerializedName("carModel")
    var carModel: String? = ""

    @SerializedName("licenseNumber")
    var licenseNumber = ""

    @SerializedName("lastDateOilChange")
    var lastDateOilChange: String? = ""

    @SerializedName("lastDateGasRefill")
    var lastDateGasRefill: String? = ""

    @SerializedName("expirationDate")
    var expirationDate: String? = ""

    @SerializedName("currentMileage")
    var currentMileage: String? = ""

}


class CurrentAssignment : Serializable {


    @SerializedName("id")
    var id = ""

    @SerializedName("assignDate")
    var assignDate = ""


}


class DashboardData : Serializable {


    @SerializedName("id")
    var id = ""

    @SerializedName("firstName")
    var firstName = ""

    @SerializedName("lastName")
    var lastName = ""

    @SerializedName("email")
    var email = ""

    @SerializedName("countryCode")
    var countryCode = ""

    @SerializedName("mobileNumber")
    var mobileNumber = ""

    @SerializedName("carID")
    var carID: String? = ""

    @SerializedName("ucarId")
    var ucarId: String? = ""

    @SerializedName("carModel")
    var carModel: String? = ""

    @SerializedName("licenseNumber")
    var licenseNumber = ""

    @SerializedName("lastDateOilChange")
    var lastDateOilChange: String? = ""

    @SerializedName("lastDateGasRefill")
    var lastDateGasRefill: String? = ""

    @SerializedName("expirationDate")
    var expirationDate: String? = ""

    @SerializedName("currentMileage")
    var currentMileage: String? = ""

    @SerializedName("driverActive")
    var driverActive: String? = ""

    @SerializedName("documentA")
    var documentA: String? = ""

    @SerializedName("documentB")
    var documentB: String? = ""

    @SerializedName("driverLicence")
    var driverLicence: String? = ""

    @SerializedName("floatingCash")
    var floatingCash: String? = ""

    @SerializedName("rating")
    var rating: String? = ""

    @SerializedName("dob")
    var dob: String? = ""

    @SerializedName("isAccepted")
    var isAccepted: Int = 0

    @SerializedName("gender")
    var gender: String? = ""

    @SerializedName("damage")
    var damage: ArrayList<Damages>? = ArrayList()

}

class Damages : Serializable {

    @SerializedName("id")
    var id: String? = ""

    @SerializedName("DamageReason")
    var damageReason: String? = ""

    var isAddedNow = false

}



