package com.app.a3d.model

class FireBaseVerificationResponse {

    var error: String? = null
    var message: String? = null
    var verificationCode: String? = null
}