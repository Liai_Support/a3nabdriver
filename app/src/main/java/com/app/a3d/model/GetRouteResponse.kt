package com.app.a3d.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class GetRouteResponse : Serializable {

    @SerializedName("error")
    var error: Boolean = true

    @SerializedName("message")
    var message: String = ""

    @SerializedName("data")
    var data: RouteData = RouteData()

}

class RouteData : Serializable {

    @SerializedName("routeDetail")
    var routeDetail: RouteDetails = RouteDetails()

    @SerializedName("routes")
    var routes: ArrayList<RoutesPartition> = ArrayList()

    @SerializedName("finalDelivery")
    var finalDelivery: Int = -1

    @SerializedName("adminNumber")
    var adminNumber: String = ""

}

class RouteDetails : Serializable {


    @SerializedName("id")
    var id: String = ""

    @SerializedName("latitude")
    var latitude: String = ""

    @SerializedName("longitude")
    var longitude: String = ""

    @SerializedName("orders")
    var orders: String = ""

    @SerializedName("pickupCount")
    var pickupCount: String = ""

    @SerializedName("dropCount")
    var dropCount: String = ""

    @SerializedName("assignDate")
    var assignDate: String = ""

    @SerializedName("isComplete")
    var isComplete: String = ""

}

class RoutesPartition() : Serializable, Parcelable {

    @SerializedName("id")
    var id: String = ""

    @SerializedName("storeId")
    var storeId: String? = null

    @SerializedName("userId")
    var userId: String? = null

    @SerializedName("orderId")
    var orderId: String? = null

    @SerializedName("orderIds")
    var orderIds: String? = ""

    @SerializedName("storeName")
    var storeName: String? = null

    @SerializedName("payType")
    var payType: String? = null

    @SerializedName("ordertax")
    var ordertax: String? = null

    @SerializedName("discountAmount")
    var discountAmount: String? = null

    @SerializedName("grandTotal")
    var grandTotal: String? = null

    @SerializedName("giveToCustomer")
    var giveToCustomer: String? = null

    @SerializedName("takeToCustomer")
    var takeToCustomer: String? = null

    @SerializedName("fastDelievryCharge")
    var fastDelievryCharge: String? = null

    @SerializedName("taxValue")
    var taxValue: String? = null

    @SerializedName("pointsAmount")
    var pointsAmount: String? = null


    @SerializedName("paidByWallet")
    var paidByWallet: String? = null


    @SerializedName("couponDiscount")
    val couponDiscount: String? = null

    @SerializedName("off_types")
    val off_types: String? = null

    @SerializedName("firstName")
    var firstName: String? = null

    @SerializedName("sortOrder")
    var sortOrder: String? = null

    @SerializedName("customerID")
    var customerID: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("countryCode")
    var countryCode: String? = null

    @SerializedName("latitude")
    var latitude: String? = null

    @SerializedName("longitude")
    var longitude: String? = null

    @SerializedName("orderOn")
    var orderOn: String? = null

    @SerializedName("deliveryDate")
    var deliveryDate: String? = null

    @SerializedName("addressPinDetails")
    var addressPinDetails: String? = null

    @SerializedName("users")
    var users: ArrayList<TopUsers>? = null


    @SerializedName("store")
    var store: ArrayList<TopStore>? = null

    @SerializedName("notes")
    var notes: ArrayList<Notes>? = null

    var isExpanded = true

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()!!
        storeId = parcel.readString()
        userId = parcel.readString()
        orderId = parcel.readString()
        storeName = parcel.readString()
        payType = parcel.readString()
        firstName = parcel.readString()
        sortOrder = parcel.readString()
        customerID = parcel.readString()
        mobileNumber = parcel.readString()
        countryCode = parcel.readString()
        latitude = parcel.readString()
        longitude = parcel.readString()
        orderOn = parcel.readString()
        deliveryDate = parcel.readString()
        addressPinDetails = parcel.readString()
        isExpanded = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(storeId)
        parcel.writeString(userId)
        parcel.writeString(orderId)
        parcel.writeString(storeName)
        parcel.writeString(payType)
        parcel.writeString(firstName)
        parcel.writeString(sortOrder)
        parcel.writeString(customerID)
        parcel.writeString(mobileNumber)
        parcel.writeString(countryCode)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
        parcel.writeString(orderOn)
        parcel.writeString(deliveryDate)
        parcel.writeString(addressPinDetails)
        parcel.writeByte(if (isExpanded) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RoutesPartition> {
        override fun createFromParcel(parcel: Parcel): RoutesPartition {
            return RoutesPartition(parcel)
        }

        override fun newArray(size: Int): Array<RoutesPartition?> {
            return arrayOfNulls(size)
        }
    }

}

class Notes() : Serializable, Parcelable {

    @SerializedName("id")
    var id: String = ""

    @SerializedName("deliveryNotes")
    var deliveryNotes: String = ""

    @SerializedName("createdAt")
    var createdAt: String = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readString().toString()
        deliveryNotes = parcel.readString().toString()
        createdAt = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(deliveryNotes)
        parcel.writeString(createdAt)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Notes> {
        override fun createFromParcel(parcel: Parcel): Notes {
            return Notes(parcel)
        }

        override fun newArray(size: Int): Array<Notes?> {
            return arrayOfNulls(size)
        }
    }


}

class TopUsers : Serializable {

    @SerializedName("u_id")
    var u_id: String? = null

    @SerializedName("firstName")
    var firstName: String? = null

    @SerializedName("customerID")
    var customerID: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("countryCode")
    var countryCode: String? = null

    @SerializedName("orderId")
    var orderId: String? = null

    @SerializedName("orderIds")
    var orderIds: String? = ""

    /*  @SerializedName("ordertax")
      var ordertax: Int? = null

      @SerializedName("discountAmount")
      var discountAmount: String? = null*/

    @SerializedName("grandTotal")
    var grandTotal: String? = null

    @SerializedName("product")
    var product: ArrayList<ProductsList>? = null


}

class TopStore() : Serializable, Parcelable {

    @SerializedName("storeId")
    var storeId: String? = null

    @SerializedName("orderIds")
    var orderIds: String? = ""

    @SerializedName("storeName")
    var storeName: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("latitude")
    var latitude: String? = null

    @SerializedName("longitude")
    var longitude: String? = null

    @SerializedName("product")
    var product: ArrayList<ProductsList>? = null

    constructor(parcel: Parcel) : this() {
        storeId = parcel.readString()
        storeName = parcel.readString()
        mobileNumber = parcel.readString()
        latitude = parcel.readString()
        longitude = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(storeId)
        parcel.writeString(storeName)
        parcel.writeString(mobileNumber)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TopStore> {
        override fun createFromParcel(parcel: Parcel): TopStore {
            return TopStore(parcel)
        }

        override fun newArray(size: Int): Array<TopStore?> {
            return arrayOfNulls(size)
        }
    }

}

class ProductsList : Serializable {


    @SerializedName("id")
    var id: String? = null

    @SerializedName("storeId")
    var storeId: String? = null

    @SerializedName("orderId")
    var orderId: String? = null

    @SerializedName("productId")
    var productId: String? = null

    @SerializedName("productName")
    var productName: String? = null

    @SerializedName("arabicName")
    var arabicName: String? = null

    @SerializedName("specialInstructions")
    var specialInstructions: String? = null

    @SerializedName("quantity")
    var quantity: String? = null

    @SerializedName("price")
    var price: String? = null

    @SerializedName("userId")
    var userId: String? = null

    @SerializedName("isPickUp")
    var isPickUp: Int = 0

    @SerializedName("receipt")
    var receipt: String? = null

    @SerializedName("totalPrice")
    var totalPrice: String? = null

    @SerializedName("discount")
    var discount: String? = null

    @SerializedName("singlePrice")
    var singlePrice: String? = null

    @SerializedName("image")
    var productImage: String? = null

    @SerializedName("cuttingStylePrice")
    var cuttingStylePrice: String? = null

    @SerializedName("boxStylePrice")
    var boxStylePrice: String? = null


}
