package com.app.a3d.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LoginResponse : Serializable {


    @SerializedName("error")
    var error = true

    @SerializedName("message")
    var message = ""

    @SerializedName("data")
    var data = LoginValue()

}

class LoginValue : Serializable {


    @SerializedName("loginDetails")
    var loginDetails = LoginDetails()

}

class LoginDetails : Serializable {

    @SerializedName("id")
    var id = 0

    @SerializedName("otp")
    var otp = 0

    @SerializedName("firstName")
    var firstName = ""

    @SerializedName("lastName")
    var lastName = ""

    @SerializedName("email")
    var email = ""

    @SerializedName("countryCode")
    var countryCode = ""

    @SerializedName("mobileNumber")
    var mobileNumber = ""

    @SerializedName("token")
    var token = ""

    @SerializedName("storeExists")
    var storeExists = ""

}