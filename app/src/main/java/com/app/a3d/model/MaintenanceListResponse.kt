package com.app.a3d.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class MaintenanceListResponse : Serializable {

    @SerializedName("error")
    var error = true

    @SerializedName("message")
    var message = ""

    @SerializedName("data")
    var data = MaintanaceList()

}

class MaintanaceList : Serializable {

    @SerializedName("list")
    var list = ArrayList<MaintenanceListData>()

}

class MaintenanceListData : Serializable {

    @SerializedName("id")
    var id = ""

    @SerializedName("nameList")
    var nameList = ""

    @SerializedName("nameListar")
    var nameListar = ""
}
