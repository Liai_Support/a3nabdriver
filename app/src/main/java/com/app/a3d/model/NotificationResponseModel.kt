package com.app.a3d.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class NotificationResponseModel : Serializable {

    @SerializedName("error")
    var error = true

    @SerializedName("message")
    var message = ""

    @SerializedName("data")
    var data: NotificationData? = null


}

class NotificationData : Serializable {

    @SerializedName("notification")
    var notification = ArrayList<NotificationList>()

}

class NotificationList : Serializable {

    @SerializedName("id")
    var id: String? = ""

    @SerializedName("assigId")
    var assigId: String? = ""

    @SerializedName("notifyType")
    var notifyType: String? = ""

    @SerializedName("created_at")
    var created_at = ""

    @SerializedName("amount")
    var amount: String? = ""

    @SerializedName("routeId")
    var routeId: String? = ""


}


/*
*   "": "false",
    "": "Success",
    "": {
        "pages": 1,
        "": [
            {
                "id": 3,
                "stoteManagerId": null,
                "driverId": 2,
                "storeId": null,
                "userId": null,
                "routeId": 17,
                "": "#Ass6808",
                "": 0,
                "": "ASSIGN_ORDER",
                "notifyMessage": "Admin assign you orders for delivery #Ass6808",
                "created_at": "2021-03-15T04:13:56.000Z",
                "updated_at": "2021-03-15T04:13:56.000Z"
            }
        ]
    }
}*/