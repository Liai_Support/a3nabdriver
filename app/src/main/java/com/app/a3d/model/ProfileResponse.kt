package com.app.a3d.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ProfileResponse : Serializable {

    @SerializedName("error")
    var error = true

    @SerializedName("message")
    var message = ""

    @SerializedName("data")
    var data = ProfileDataData()

}

class ProfileDataData : Serializable {
    @SerializedName("profile")
    var profile = ProfileData()
}

class ProfileData : Serializable {


    @SerializedName("id")
    var id = ""

    @SerializedName("firstName")
    var firstName = ""

    @SerializedName("lastName")
    var lastName = ""

    @SerializedName("email")
    var email = ""

    @SerializedName("countryCode")
    var countryCode = ""

    @SerializedName("mobileNumber")
    var mobileNumber = ""

    @SerializedName("carID")
    var carID: String? = ""

    @SerializedName("carModel")
    var carModel: String? = ""

    @SerializedName("licenseNumber")
    var licenseNumber = ""

    @SerializedName("lastDateOilChange")
    var lastDateOilChange: String? = ""

    @SerializedName("lastDateGasRefill")
    var lastDateGasRefill: String? = ""

    @SerializedName("expirationDate")
    var expirationDate: String? = ""

    @SerializedName("currentMileage")
    var currentMileage: String? = ""

    @SerializedName("driverActive")
    var driverActive: String? = ""

    @SerializedName("documentA")
    var documentA: String? = ""

    @SerializedName("documentB")
    var documentB: String? = ""

    @SerializedName("driverLicence")
    var driverLicence: String? = ""

    @SerializedName("assignment")
    var assignment: String? = ""


    @SerializedName("rating")
    var rating: String? = ""

    @SerializedName("dob")
    var dob: String? = ""

    @SerializedName("gender")
    var gender: String? = ""

}

