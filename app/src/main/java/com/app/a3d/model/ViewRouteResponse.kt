package com.app.a3d.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ViewRouteResponse : Serializable {

    @SerializedName("error")
    var error: Boolean = true

    @SerializedName("message")
    var message: String = ""

    @SerializedName("data")
    var data: PastRouteData = PastRouteData()

}

class PastRouteData : Serializable {

    @SerializedName("routeDetail")
    var routeDetail: PastRouteDetails = PastRouteDetails()

    @SerializedName("routes")
    var routes: ArrayList<PastRoutesPartition> = ArrayList()

    @SerializedName("finalDelivery")
    var finalDelivery: Int = -1

    @SerializedName("adminNumber")
    var adminNumber: String = ""

}

class PastRouteDetails : Serializable {


    @SerializedName("id")
    var id: String = ""

    @SerializedName("latitude")
    var latitude: String = ""

    @SerializedName("longitude")
    var longitude: String = ""

    @SerializedName("orders")
    var orders: String = ""

    @SerializedName("pickupCount")
    var pickupCount: String = ""

    @SerializedName("dropCount")
    var dropCount: String = ""

    @SerializedName("assignDate")
    var assignDate: String = ""

    @SerializedName("isComplete")
    var isComplete: String = ""

}

class PastRoutesPartition : Serializable {

    @SerializedName("id")
    var id: String = ""

    @SerializedName("storeId")
    var storeId: String? = null

    @SerializedName("userId")
    var userId: String? = null

    @SerializedName("orderId")
    var orderId: String? = null

    @SerializedName("storeName")
    var storeName: String? = null

    @SerializedName("firstName")
    var firstName: String? = null

    @SerializedName("sortOrder")
    var sortOrder: String? = null

    @SerializedName("customerID")
    var customerID: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("countryCode")
    var countryCode: String? = null

    @SerializedName("latitude")
    var latitude: String? = null

    @SerializedName("longitude")
    var longitude: String? = null

    @SerializedName("orderOn")
    var orderOn: String? = null

    @SerializedName("deliveryDate")
    var deliveryDate: String? = null

    @SerializedName("addressPinDetails")
    var addressPinDetails: String? = null

    @SerializedName("users")
    var users: ArrayList<PastTopUsers>? = null


    @SerializedName("store")
    var store: ArrayList<PastTopStore>? = null

    @SerializedName("notes")
    var notes: ArrayList<PastNotes>? = null

    var isExpanded = false


}

class PastNotes : Serializable {

    @SerializedName("id")
    var id: String = ""

    @SerializedName("deliveryNotes")
    var deliveryNotes: String = ""

    @SerializedName("createdAt")
    var createdAt: String = ""


}

class PastTopUsers : Serializable {

    @SerializedName("u_id")
    var u_id: String? = null

    @SerializedName("firstName")
    var firstName: String? = null

    @SerializedName("customerID")
    var customerID: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("countryCode")
    var countryCode: String? = null

    @SerializedName("product")
    var product: ArrayList<PastProductsList>? = null


}

class PastTopStore() : Serializable {

    @SerializedName("storeId")
    var storeId: String? = null

    @SerializedName("storeName")
    var storeName: String? = null

    @SerializedName("mobileNumber")
    var mobileNumber: String? = null

    @SerializedName("latitude")
    var latitude: String? = null

    @SerializedName("longitude")
    var longitude: String? = null

    @SerializedName("product")
    var product: ArrayList<PastProductsList>? = null


}

class PastProductsList : Serializable {


    @SerializedName("id")
    var id: String? = null

    @SerializedName("storeId")
    var storeId: String? = null

    @SerializedName("orderId")
    var orderId: String? = null

    @SerializedName("productId")
    var productId: String? = null

    @SerializedName("productName")
    var productName: String? = null

    @SerializedName("quantity")
    var quantity: String? = null

    @SerializedName("price")
    var price: String? = null

    @SerializedName("userId")
    var userId: String? = null

    @SerializedName("isPickUp")
    var isPickUp: Int = 0

    @SerializedName("receipt")
    var receipt: String? = null

    @SerializedName("totalPrice")
    var totalPrice: String? = null

    @SerializedName("discount")
    var discount: String? = null

    @SerializedName("singlePrice")
    var singlePrice: String? = null

    @SerializedName("productImage")
    var productImage: String? = null


}
