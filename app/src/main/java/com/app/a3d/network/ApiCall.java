package com.app.a3d.network;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.a3d.R;
import com.app.a3d.background.MyApp;
import com.app.a3d.utils.SharedHelper;
import com.app.a3d.utils.Utils;
import com.app.a3d.view.activity.LoginActivity;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ApiCall {

    private static String TAG = ApiCall.class.getSimpleName();
    private static int MY_SOCKET_TIMEOUT_MS = 10000;


    public static void PostMethod(InputForAPI input, final ResponseHandler volleyCallback) {
        final String url = input.getUrl();
        final Context context = input.getContext();
        JSONObject params = input.getJsonObject();
        final HashMap<String, String> headers = input.getHeaders();

        if (Utils.isNetworkConnected(context)) {

            Utils.log(TAG, "url:" + url + "--input: " + params + "--headers: " + headers.toString());
            Utils.log(TAG, "json:" + input.getJsonObject());
            final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Utils.log(TAG, "url:" + url + ",response: " + response);
                            volleyCallback.setDataResponse(response);
                        }
                    }, error -> {
                Utils.log(TAG, "url:" + url + ", onErrorResponse: " + error);
                if (error instanceof TimeoutError) {
                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_time_out));
                } else if (error instanceof NoConnectionError) {
                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));
                } else if (error instanceof AuthFailureError) {
                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_authentication));
                    moveToLoginActivity(input.getContext());
//                            exitApp(context);
                } else if (error instanceof ServerError) {
                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_server));
                } else if (error instanceof NetworkError) {
                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_network));
                } else if (error instanceof ParseError) {
                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_parse));
                } else {
                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_time_out));
                }

            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return headers;
                }
            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApp.getInstance().addToRequestQueue(jsonObjReq);

        } else {
            volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));
        }
    }


    public static void GetMethod(InputForAPI input, final ResponseHandler volleyCallback) {
        final String url = input.getUrl();
        final Context context = input.getContext();
        final HashMap<String, String> headers = input.getHeaders();
        if (Utils.isNetworkConnected(context)) {
            Utils.log(TAG, "url:" + url + "--headers: " + headers.toString());

            final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Utils.log(TAG, "url:" + url + ",response: " + response);
                            volleyCallback.setDataResponse(response);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utils.log(TAG, "url:" + url + ", onErrorResponse: " + error);
                    if (error instanceof TimeoutError) {
                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_time_out));
                    } else if (error instanceof NoConnectionError) {
                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));
                    }  else if (error instanceof AuthFailureError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_authentication));
                        moveToLoginActivity(input.getContext());
//                        exitApp(context);

                    } else if (error instanceof ServerError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_server));

                    } else if (error instanceof NetworkError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_network));

                    } else if (error instanceof ParseError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.error_parse));

                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return headers;
                }
            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            MyApp.getInstance().addToRequestQueue(jsonObjReq);

        } else {
            volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));
        }
    }


    public static void fileUpload(final InputForAPI input, final ResponseHandler volleyCallback) {

        final String url = input.getUrl();
        final Context context = input.getContext();

        Utils.log(TAG, "--method:post " + "--url:" + url + " --input: " + input.getJsonObject() + " --headers: " + input.getHeaders().toString());

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                String resultResponse = new String(response.data);
                try {
                    JSONObject result = new JSONObject(resultResponse);
                    Utils.log(TAG, "url:" + url + ",response: " + result);

                    volleyCallback.setDataResponse(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof TimeoutError) {
                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_time_out));
                } else if (error instanceof NoConnectionError) {
                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_no_internet_connection));
                } else if (error instanceof AuthFailureError) {

                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_authentication));
//                    exitApp(context);
                } else if (error instanceof ServerError) {

                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_server));

                } else if (error instanceof NetworkError) {

                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_network));

                } else if (error instanceof ParseError) {

                    volleyCallback.setResponseError(context.getResources().getString(R.string.error_parse));
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("type", input.getJsonObject().optString("type"));
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                try {
                    params.put("file", new DataPart(input.getFile().getName(), ImageUtils.convertImagetoByteData(input.getFile())));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return params;
            }
        };

        MyApp.getInstance().addToRequestQueue(multipartRequest);
    }


    private static void moveToLoginActivity(Context context) {


        new SharedHelper(context).clearValues();

        Intent intent = new Intent(context, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
        ((Activity) context).finish();

    }


    public interface ResponseHandler {

        public void setDataResponse(JSONObject response);

        public void setResponseError(String error);

    }

}
