package com.app.a3d.network;

import android.content.Context;


import com.app.a3d.utils.Constants;
import com.app.a3d.utils.SharedHelper;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

public class InputForAPI {

    private JSONObject jsonObject = new JSONObject();
    private String url = "";
    private HashMap<String, String> headers = new HashMap<>();
    private HashMap<String, String> stringInput = new HashMap<>();
    private File file = null;

    private Context context;

    public HashMap<String, String> getStringInput() {
        return stringInput;
    }

    public void setStringInput(HashMap<String, String> stringInput) {
        this.stringInput = stringInput;
    }

    public InputForAPI(Context context) {
        this.context = context;
        SharedHelper sharedHelper = new SharedHelper(context);
        headers.put(Constants.ApiKeys.LANG, "en");
        headers.put(Constants.ApiKeys.AUTHORIZATION, sharedHelper.getAuthToken());
        headers.put(Constants.ApiKeys.ROLE, "driver");

    }

    public JSONObject getJsonObject() {

        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    HashMap<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(HashMap<String, String> headers) {
        this.headers = headers;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
