package com.app.a3d.network;


import com.app.a3d.BuildConfig;
import com.app.a3d.R;
import com.app.a3d.background.MyApp;

public class URLHelper {

    private static final String BASE_URL = BuildConfig.BASE_URL;
    private static final String USER_BASE_URL = BASE_URL + "/driver/";


    public static final String UPDATEDEVICETOKEN = USER_BASE_URL + "updateDeviceToken";
    public static final String LOGIN = USER_BASE_URL + "login";
    public static final String GETPROFILE = USER_BASE_URL + "getProfile";
    public static final String UPDATEPROFILE = USER_BASE_URL + "updateProfile";
    public static final String DASHBOARD = USER_BASE_URL + "dashboard";
    public static final String UPDATEDRIVERSTATUS = USER_BASE_URL + "updateDriverStatus";
    public static final String UPLOADDOCUMENT = USER_BASE_URL + "uploadDocument";
    public static final String MAINTANANCELIST = USER_BASE_URL + "maintenanceList";
    public static final String ADDMAINTANANCE = USER_BASE_URL + "addMaintenance";
    public static final String RETURNCAR = USER_BASE_URL + "returnCar";
    public static final String GETROUTES = USER_BASE_URL + "getRoute";
    public static final String UPDATEORDERSTATUS = USER_BASE_URL + "updateOrderStatus";
    public static final String UPLOADRECIPT = USER_BASE_URL + "updateReceipt";
    public static final String GETASSIGNMENT = USER_BASE_URL + "getAssignment";
    public static final String VERIFYOTP =  BASE_URL+"/user/"+"verifyOtp";;


    public static final String MOVETOLAST = USER_BASE_URL + "moveToLast";
    public static final String MAKEDELIVERY = USER_BASE_URL + "makeDelivery";
    public static final String FINALDELIVERYCALL = USER_BASE_URL + "finalDeliveryCall";
    public static final String ACCEPTCAR = USER_BASE_URL + "acceptCar";
    public static final String UPDATELATLNG = USER_BASE_URL + "updatelatlng";
    public static final String VIEWORDERDETAILS = USER_BASE_URL + "viewOrderDetails";
    public static final String DRIVERNOTIFICATION = USER_BASE_URL + "driverNotification";


    public static final String GOOGLE_API_BASE_URL = "https://maps.googleapis.com/maps/api/";
    public static final String GOOGLE_API_DIRECTION_BASE_URL = GOOGLE_API_BASE_URL + "directions/json?";
    public static final String GOOGLE_API_AUTOCOMPLETE_BASE_URL = GOOGLE_API_BASE_URL + "place/autocomplete/json?";
    public static final String GOOGLE_API_PLACE_DETAILS_BASE_URL = GOOGLE_API_BASE_URL + "place/details/json?";
    public static final String GOOGLE_API_STATIC_MAP_BASE_URL = GOOGLE_API_BASE_URL + "staticmap?";
    public static final String GOOGLE_API_GEOCODE_BASE_URL = GOOGLE_API_BASE_URL + "geocode/json?";


    public static String getAddress(
            String latitude,
            String longitude
    ) {

        return (GOOGLE_API_GEOCODE_BASE_URL + "latlng=" + latitude + ","
                + longitude + "&sensor=true&key=" + MyApp.getInstance().getApplicationContext().getString(
                R.string.map_api_key
        ));
    }
}
