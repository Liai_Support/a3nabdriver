package com.app.a3d.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3d.model.AssignmentResponse;
import com.app.a3d.model.CommonResponse;
import com.app.a3d.model.DashboardResponse;
import com.app.a3d.model.GetRouteResponse;
import com.app.a3d.model.MaintenanceListResponse;
import com.app.a3d.model.NotificationResponseModel;
import com.app.a3d.model.ProfileResponse;
import com.app.a3d.model.ViewRouteResponse;
import com.app.a3d.network.ApiCall;
import com.app.a3d.network.InputForAPI;
import com.google.gson.Gson;

import org.json.JSONObject;

public class CommonRepository {

    public LiveData<DashboardResponse> dashBoardDetails(InputForAPI inputForAPI) {

        MutableLiveData<DashboardResponse> responseValue = new MutableLiveData<>();

        ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                DashboardResponse result = gson.fromJson(response.toString(), DashboardResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                DashboardResponse result = new DashboardResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }

    public LiveData<ProfileResponse> getProfileDetails(InputForAPI inputForAPI) {

        MutableLiveData<ProfileResponse> responseValue = new MutableLiveData<>();

        ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                ProfileResponse result = gson.fromJson(response.toString(), ProfileResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                ProfileResponse result = new ProfileResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }

    public LiveData<CommonResponse> uploadDocument(InputForAPI inputForAPI) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }


    public LiveData<CommonResponse> updateProfile(InputForAPI inputForAPI) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }

    public LiveData<CommonResponse> updateStatus(InputForAPI inputForAPI) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }

    public MutableLiveData<MaintenanceListResponse> getMaintenanceList(InputForAPI inputForAPI) {

        MutableLiveData<MaintenanceListResponse> responseValue = new MutableLiveData<>();

        ApiCall.GetMethod(inputForAPI, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                MaintenanceListResponse result = gson.fromJson(response.toString(), MaintenanceListResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                MaintenanceListResponse result = new MaintenanceListResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }

    public MutableLiveData<CommonResponse> addMaintenance(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }

    public MutableLiveData<CommonResponse> returnCar(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }


    public MutableLiveData<GetRouteResponse> getRoutes(InputForAPI inputs) {

        MutableLiveData<GetRouteResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                GetRouteResponse result = gson.fromJson(response.toString(), GetRouteResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                GetRouteResponse result = new GetRouteResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }


    public MutableLiveData<ViewRouteResponse> getViewRoutes(InputForAPI inputs) {

        MutableLiveData<ViewRouteResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                ViewRouteResponse result = gson.fromJson(response.toString(), ViewRouteResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                ViewRouteResponse result = new ViewRouteResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }


    public MutableLiveData<CommonResponse> updateProductStatus(InputForAPI inputs) {

        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;

    }

    public MutableLiveData<CommonResponse> uploadRecipt(InputForAPI inputs) {
        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }


    public MutableLiveData<AssignmentResponse> getAssignmetnList(InputForAPI inputs) {
        MutableLiveData<AssignmentResponse> responseValue = new MutableLiveData<>();

        ApiCall.GetMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                AssignmentResponse result = gson.fromJson(response.toString(), AssignmentResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                AssignmentResponse result = new AssignmentResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }

    public MutableLiveData<CommonResponse> makeDelivery(InputForAPI inputs) {
        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }

    public MutableLiveData<CommonResponse> moveToLast(InputForAPI inputs) {
        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }


    public MutableLiveData<CommonResponse> finalDelivery(InputForAPI inputs) {
        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;
    }

    public MutableLiveData<CommonResponse> acceptCar(InputForAPI inputs) {


        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });

        return responseValue;

    }

    public void updateLatLng(InputForAPI inputs) {


        MutableLiveData<CommonResponse> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                CommonResponse result = gson.fromJson(response.toString(), CommonResponse.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                CommonResponse result = new CommonResponse();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });


    }


    public void updateDeviceToken(InputForAPI inputs) {


        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {


            }

            @Override
            public void setResponseError(String error) {


            }
        });


    }


    public MutableLiveData<NotificationResponseModel> getNotificationList(InputForAPI inputs) {

        MutableLiveData<NotificationResponseModel> responseValue = new MutableLiveData<>();

        ApiCall.PostMethod(inputs, new ApiCall.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {

                Gson gson = new Gson();
                NotificationResponseModel result = gson.fromJson(response.toString(), NotificationResponseModel.class);
                responseValue.setValue(result);

            }

            @Override
            public void setResponseError(String error) {

                NotificationResponseModel result = new NotificationResponseModel();
                result.setError(true);
                result.setMessage(error);
                responseValue.setValue(result);

            }
        });


        return responseValue;
    }
}
