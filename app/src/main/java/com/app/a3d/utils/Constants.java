package com.app.a3d.utils;

import android.Manifest;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.app.a3d.BuildConfig;

public class Constants {
    public static class ApiKeys {

        public static final String LANG = "lang";
        public static final String AUTHORIZATION = "authorization";
        public static final String ROLE = "role";
        public static final String IMAGEURL = "imageUrl";
    }

    public static class Permissions {

        public static final String[] LOCATIONPERMISSION_LIST = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION};

        public static final int LOCATIONREQUEST_CODE = 201;
    }

    public static class RequestCode {
        public static final int GPS_REQUEST = 400;
        public static final int ADDRESS_REQUEST = 401;

        public static final int READ_STORAGE_PERMISSIONS = 194;
        public static final int CAMERA_STORAGE_PERMISSIONS = 195;

        public static final int CAMERA_INTENT = 201;
        public static final int GALLERY_INTENT = 202;
    }

    //image Upload from amazon s3
    public static class AWS {
        public static final String POOL_ID = BuildConfig.POOL_ID;
        public static final String BASE_S3_URL = BuildConfig.BASE_S3_URL;
        public static final String ENDPOINT = BuildConfig.ENDPOINT;
        public static final String BUCKET_NAME = BuildConfig.BUCKET_NAME;
        public static final Regions REGION = Regions.US_EAST_1;
    }

    public static class IntentPermissionCode {
        public static final int COARSE_LOCATION_PERMISSIONS = 1;
        public static final int REQUEST_LOCATION = 2;
        public static final int SOURCE_REQUEST_CODE = 3;
        public static final int DESTINATION_REQUEST_CODE = 4;
        public static final int RATING_STATUS = 5;
        public static final int IMAGE_PICKER = 6;
        public static final int EXTERNAL_STORAGE_PERMISSION = 7;
        public static final int CALL_PHONE = 8;
        public static final int STRIPE_PAYMENT = 9;
        public static final int ADD_CARD = 10;
        public static final int PAYMENT_SELECTION = 11;
        public static final int UPDATE_CHECK = 12;
        public static final int ADD_WALLET = 13;

    }
}
