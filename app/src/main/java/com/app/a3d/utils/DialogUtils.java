package com.app.a3d.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.app.a3d.R;
import com.app.a3d.interfaces.AddLocationUpdates;


import org.w3c.dom.Text;

import java.io.File;
import java.util.ArrayList;

import static com.app.a3d.utils.Constants.RequestCode.GALLERY_INTENT;
import static com.app.a3d.utils.Constants.RequestCode.READ_STORAGE_PERMISSIONS;

public class DialogUtils {

    static Dialog loaderDialog;


    public static void showLoader(Context context) {

        if (loaderDialog != null) {
            if (loaderDialog.isShowing()) {
                loaderDialog.dismiss();
            }
        }

        loaderDialog = new Dialog(context);
        loaderDialog.setCancelable(false);
        loaderDialog.setCanceledOnTouchOutside(false);
        loaderDialog.getWindow().setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT
        );
        loaderDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_loader, null);
        if (view != null) {
            loaderDialog.setContentView(view);
        }

        ((LottieAnimationView) loaderDialog.findViewById(R.id.anim_view)).playAnimation();

        if (!loaderDialog.isShowing()) {
            loaderDialog.show();
        }

    }

    public static void dismissLoader() {
        if (loaderDialog != null) {
            if (loaderDialog.isShowing()) {
                loaderDialog.dismiss();
            }
        }
    }

    public static void showImage(
            Context context,
            String url
    ) {
        ImageView image = new ImageView(context);
        Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(image);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

//        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.8);
//        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.6);

        dialog.getWindow().setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT);

        image.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Utils.loadImage(image, url, R.mipmap.ic_launcher);


        dialog.show();

    }

    public static Dialog getMaintenanceDialog(Context context) {
        Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_request_maintenance);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.8);
//        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.6);

        dialog.getWindow().setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT);
        return dialog;

    }

    public static Dialog getReturnCarDialog(Context context) {
        Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_request_return_car);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.8);
//        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.6);

        dialog.getWindow().setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        return dialog;

    }


    public static void viewLocationNotes(Context context, String date, String notes) {
        Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_view_location_notes);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.8);
//        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.6);

        dialog.getWindow().setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT);


        ((TextView) dialog.findViewById(R.id.date)).setText(context.getString(R.string.added_on) + " " + date);
        ((TextView) dialog.findViewById(R.id.notes)).setText(notes);

        dialog.show();

    }


    public static void showLocationNotes(Context context, AddLocationUpdates updates) {

        String[] isHelpful = {""};

        Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_add_location_notes);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.95);
//        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.6);

        dialog.getWindow().setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        CardView save = dialog.findViewById(R.id.save);
        CardView yes = dialog.findViewById(R.id.cardView3);
        CardView no = dialog.findViewById(R.id.no);

        EditText message = dialog.findViewById(R.id.message);
        TextView yesTxt = dialog.findViewById(R.id.mileage);
        TextView noTxt = dialog.findViewById(R.id.noTxt);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!isHelpful[0].equalsIgnoreCase("")) {
                    updates.updateNotes(isHelpful[0], message.getText().toString().trim());
                    dialog.dismiss();
                }
                else {
                    Utils.showSnack(v,context.getString(R.string.checkallfields));
                }

            }
        });

        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isHelpful[0] = "Yes";
                yes.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
                no.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white));

                yesTxt.setTextColor(ContextCompat.getColor(context, R.color.white));
                noTxt.setTextColor(ContextCompat.getColor(context, R.color.edit_text_color));
            }
        });

        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isHelpful[0] = "No";
                yes.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white));
                no.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));

                yesTxt.setTextColor(ContextCompat.getColor(context, R.color.edit_text_color));
                noTxt.setTextColor(ContextCompat.getColor(context, R.color.white));

            }
        });


        dialog.show();

    }


    public static Dialog callDialog(Context context, String header, String userNumber, String adminNumber, boolean isShop) {
        Dialog dialog = new Dialog(context);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_call);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(
                        ContextCompat.getColor(
                                context,
                                R.color.transparent
                        )
                )
        );

        int width = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.9);
//        int height = (int) (context.getResources().getDisplayMetrics().widthPixels * 0.6);

        dialog.getWindow().setLayout(width, ConstraintLayout.LayoutParams.WRAP_CONTENT);


        TextView headerTxt = dialog.findViewById(R.id.textView20);
        CardView callCustomer = dialog.findViewById(R.id.callCustomer);
        TextView customerText = dialog.findViewById(R.id.textView28);
        CardView callAdmin = dialog.findViewById(R.id.callAdmin);

        headerTxt.setText(header);

        if (isShop) {
            customerText.setText(context.getString(R.string.call_shop));
        } else {
            customerText.setText(context.getString(R.string.call_customer));
        }
        callCustomer.setOnClickListener(v -> {

            if (userNumber.equalsIgnoreCase("")) {
                return;
            }

            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.CALL_PHONE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.CALL_PHONE},
                        100);

                // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            } else {
                //You already have permission
                Log.d("no1",""+userNumber);
                Log.d("xczx",""+adminNumber);
                try {
                    Intent intent = new Intent(Intent.ACTION_CALL,
                            Uri.parse("tel:" + "+" + userNumber));
                    context.startActivity(intent);
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }
        });

        callAdmin.setOnClickListener(v -> {

            if (adminNumber.equalsIgnoreCase("")) {
                return;
            }

            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.CALL_PHONE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.CALL_PHONE},
                        100);

                // MY_PERMISSIONS_REQUEST_CALL_PHONE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            } else {
                //You already have permission
                try {
                    Intent intent = new Intent(Intent.ACTION_CALL,
                            Uri.parse("tel:" + "+" + adminNumber));
                    context.startActivity(intent);
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }

        });

        dialog.show();
        return dialog;

    }

}
