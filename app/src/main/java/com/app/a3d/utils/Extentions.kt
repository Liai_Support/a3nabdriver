package com.app.a3d.utils

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.app.a3d.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.snackbar.Snackbar

fun ImageView.loadImage(resourse: Int) {
    if (this.context != null) {
        this.setImageDrawable(ContextCompat.getDrawable(this.context, resourse))
    }
}

fun FragmentManager.navigateToFragment(
    id: Int,
    fragment: Fragment?
) {
    fragment?.let {
        val fragmentTransaction =
            this.beginTransaction()
        fragmentTransaction.replace(id, fragment)
        fragmentTransaction.addToBackStack(fragment.javaClass.simpleName)
        fragmentTransaction.commit()
    }
}


fun <T> Context.openActivity(it: Class<T>) {
    val intent = Intent(this, it)
    startActivity(intent)
}

fun <T> Context.openActivity(it: Class<T>, bundle: Bundle) {
    val intent = Intent(this, it)
    intent.putExtras(bundle)
    startActivity(intent)
}


fun <T> Context.openNewTaskActivity(it: Class<T>) {
    val intent = Intent(this, it)
    intent.flags =
        Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
    startActivity(intent)
}

fun View.showSnack(string: String?) {

    string?.let {
        val snack = Snackbar.make(this, string, Snackbar.LENGTH_LONG)
        val view: View = snack.view
        view.setBackgroundColor(ContextCompat.getColor(this.context, R.color.black))
        val params = view.layoutParams as FrameLayout.LayoutParams
//    params.gravity = Gravity.TOP
        view.layoutParams = params
        snack.show()
    }

}

/*fun Context.isNetworkConnected(): Boolean {
    val connectivityManager: ConnectivityManager? =
        getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    var isConnected = false
    if (connectivityManager != null) {
        val activeNetwork = connectivityManager.activeNetworkInfo
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }
    return isConnected
}*/

fun ImageView.loadImage(imageUrl: String?) {
    if (imageUrl == null) {
        return
    }

    Glide.with(this.context)
        .load(imageUrl)
        .apply(
            RequestOptions()
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
        )
        .into(this)


}

fun String.isValidPhoneNumber() = this.length >= 8

fun Int.format2Digit() = String.format("%02d", this)