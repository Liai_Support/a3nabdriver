package com.app.a3d.utils;


public class SessionManager {

    private static SessionManager instance = null;

    public static SessionManager getInstance() {
        if (instance == null) {
            instance = new SessionManager();
        }

        return instance;
    }

    private boolean isRegisterLaterFlow;
    private String countryCode = "";
    private String mobileNumber = "";
    private String verificationCode = "";

    public boolean isRegisterLaterFlow() {
        return isRegisterLaterFlow;
    }

    public void setRegisterLaterFlow(boolean registerLaterFlow) {
        isRegisterLaterFlow = registerLaterFlow;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }
}
