package com.app.a3d.utils;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

public class SharedHelper {

    private SharedPreference sharedPreference;


    public SharedHelper(Context context) {
        sharedPreference = new SharedPreference(context);
    }


    private String authToken;
    private String currentAssignmentRouteId;
    private String selectedLanguage;
    private String firebaseToken;
    private boolean showSoundDialog;

    private String userId;
    private String firstName;
    private String lastName;
    private String email;
    private String latitude;
    private String longitude;
    private String countryCode;
    private String mobileNumber;
    private String profilePic;
    private Boolean isLoggedIn;
    private ArrayList<String> addedCategory;

    private Boolean isAddAddress;

    public Boolean getIsAddAddress() {
        isAddAddress = sharedPreference.getBoolean("isAddAddress");
        return isAddAddress;
    }

    public void setIsAddAddress(Boolean isAddAddress) {
        sharedPreference.putBoolean("isAddAddress", isAddAddress);
        isAddAddress = isAddAddress;
    }

    public Boolean getLoggedIn() {
        isLoggedIn = sharedPreference.getBoolean("loggedIn");
        return isLoggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        sharedPreference.putBoolean("loggedIn", loggedIn);
        isLoggedIn = loggedIn;
    }

    public String getcurrentAssignmentRouteId() {
        currentAssignmentRouteId = sharedPreference.getKey("currentAssignmentRouteId");
        return currentAssignmentRouteId;
    }

    public void setcurrentAssignmentRouteId(String currentAssignmentRouteId) {
        this.sharedPreference.putKey("currentAssignmentRouteId", currentAssignmentRouteId);
        this.currentAssignmentRouteId = currentAssignmentRouteId;
    }

    public String getAuthToken() {
        authToken = sharedPreference.getKey("authToken");
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.sharedPreference.putKey("authToken", authToken);
        this.authToken = authToken;
    }

    public String getSelectedLanguage() {
        selectedLanguage = sharedPreference.getKey("selectedLanguage");
        if (selectedLanguage.equals("")) {
            return "en";
        }
        return selectedLanguage;
    }

    public void setSelectedLanguage(String selectedLanguage) {
        this.sharedPreference.putKey("selectedLanguage", selectedLanguage);
        this.selectedLanguage = selectedLanguage;
    }

    public String getFirebaseToken() {
        firebaseToken = sharedPreference.getKey("firebaseToken");
        return firebaseToken;
    }

    public void setFirebaseToken(String firebaseToken) {
        this.sharedPreference.putKey("firebaseToken", firebaseToken);
        this.firebaseToken = firebaseToken;
    }

    public String getUserId() {
        userId = sharedPreference.getKey("userId");
        return userId;
    }

    public void setUserId(String userId) {
        this.sharedPreference.putKey("userId", userId);
        this.userId = userId;
    }

    public String getFirstName() {
        firstName = sharedPreference.getKey("firstName");
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName != null)
            this.sharedPreference.putKey("firstName", firstName);
        this.firstName = firstName;
    }

    public String getLastName() {
        lastName = sharedPreference.getKey("lastName");
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName != null)
            this.sharedPreference.putKey("lastName", lastName);
        this.lastName = lastName;
    }

    public String getEmail() {
        email = sharedPreference.getKey("email");
        return email;
    }

    public void setEmail(String email) {
        if (email != null)
            this.sharedPreference.putKey("email", email);
        this.email = email;
    }

    public String getLatitude() {
        latitude = sharedPreference.getKey("latitude");
        return latitude;
    }

    public void setLatitude(String latitude) {
        if (latitude != null)
            this.sharedPreference.putKey("latitude", latitude);
        this.latitude = latitude;
    }

    public String getLongitude() {
        longitude = sharedPreference.getKey("longitude");
        return longitude;
    }

    public void setLongitude(String longitude) {
        if (longitude != null)
            this.sharedPreference.putKey("longitude", longitude);
        this.longitude = longitude;
    }

    public String getCountryCode() {
        countryCode = sharedPreference.getKey("countryCode");
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        if (countryCode != null)
            this.sharedPreference.putKey("countryCode", countryCode);
        this.countryCode = countryCode;
    }

    public String getMobileNumber() {
        mobileNumber = sharedPreference.getKey("mobileNumber");
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        if (mobileNumber != null)
            this.sharedPreference.putKey("mobileNumber", mobileNumber);
        this.mobileNumber = mobileNumber;
    }

    public String getProfilePic() {
        profilePic = sharedPreference.getKey("profilePic");
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        if (profilePic != null)
            this.sharedPreference.putKey("profilePic", profilePic);
        this.profilePic = profilePic;
    }




    public ArrayList<String> getAddedCategory() {

        ArrayList<String> playersList = new Gson().fromJson(sharedPreference.getKey("addedCategory"),
                new TypeToken<ArrayList<String>>() {
                }.getType());

        if (playersList == null) {
            playersList = new ArrayList<>();
        }
        this.addedCategory = playersList;
        return playersList;
    }

    public void setAddedCategory(ArrayList<String> addedCategory) {

        String listString = new Gson().toJson(
                addedCategory,
                new TypeToken<ArrayList<String>>() {
                }.getType());

        this.sharedPreference.putKey("addedCategory", listString);
        this.addedCategory = addedCategory;
    }

    public void clearValues(){
        sharedPreference.clearAllValues();
    }

    public boolean isShowSoundDialog() {
        showSoundDialog = sharedPreference.getBoolean("showSoundDialog");
        return showSoundDialog;
    }

    public void setShowSoundDialog(boolean showSoundDialog) {
        this.sharedPreference.putBoolean("showSoundDialog", showSoundDialog);
        this.showSoundDialog = showSoundDialog;
    }
}
