package com.app.a3d.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.app.a3d.BuildConfig;
import com.app.a3d.R;
import com.app.a3d.network.InputForAPI;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Utils {

    public static void showSnack(View view, String text) {
        Snackbar.make(view, text, Snackbar.LENGTH_LONG).show();
    }


    public static String formatedValues(Double value) {
        return String.format(Locale.ENGLISH, "%.2f", value);
    }

    public static String formatedValues(String value) {
        if (!value.equalsIgnoreCase(""))
            return String.format(Locale.ENGLISH, "%.2f", Double.parseDouble(value));
        else return "0.00";
    }


    public static void navigateToFragment(
            FragmentManager manager,
            int id,
            Fragment fragment
    ) {
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.replace(id, fragment);
        fragmentTransaction.addToBackStack(fragment.getClass().getSimpleName());
        fragmentTransaction.commit();

    }

    public static String formatedValues(int value) {
        return String.format(Locale.ENGLISH,"%02d", value);
    }


    public static void log(String TAG, String content) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, content);
        }
    }

    public static boolean isNetworkConnected(Context context) {
        boolean isConnected;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo activeWIFIInfo = connectivityManager
                .getNetworkInfo(connectivityManager.TYPE_WIFI);

        if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
            isConnected = true;
        } else {
            isConnected = false;
        }
        return isConnected;
    }

    public static InputForAPI getInputs(Context context, JSONObject jsonObject, String method) {

        InputForAPI inputForAPI = new InputForAPI(context);
        inputForAPI.setJsonObject(jsonObject);
        inputForAPI.setUrl(method);

        return inputForAPI;
    }

    public static void loadImage(ImageView imageview, String imageUrl) {
        if (imageUrl == null) {
            return;
        }

        Glide.with(imageview.getContext())
                .load(imageUrl)
                .apply(new RequestOptions()
                        .placeholder(R.mipmap.ic_launcher)
                        .error(R.mipmap.ic_launcher))
                .into(imageview);


    }

    public static void loadImage(ImageView imageview, String imageUrl, int placeHolder) {
        if (imageUrl == null) {
            return;
        }

        Glide.with(imageview.getContext())
                .load(imageUrl)
                .apply(new RequestOptions()
                        .placeholder(placeHolder)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .error(R.mipmap.ic_launcher))
                .into(imageview);


    }

    public static void loadImageBanner(ImageView imageview, String imageUrl) {
        if (imageUrl == null) {
            return;
        }

        Glide.with(imageview.getContext())
                .load(imageUrl)
                .apply(new RequestOptions()
//                        .placeholder(R.drawable.demo)
                        .error(R.mipmap.ic_launcher))
                .into(imageview);


    }

    public static SpannableStringBuilder getColoredAndBoldString(Context context, String content, int color) {

        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        SpannableString spannableString = new SpannableString(content);
//        StyleSpan styleSpan = new StyleSpan(Typeface.BOLD);
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(ContextCompat.getColor(context, color));
//        spannableString.setSpan(styleSpan, 0, content.length(), 0);
        spannableString.setSpan(colorSpan, 0, content.length(), 0);
        stringBuilder.append(spannableString);

        return stringBuilder;
    }

    public static String convertDate(Date date, String format) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.ENGLISH);
        return simpleDateFormat.format(date);

    }

    public static String convertDate(String date, String inputFormat, String outputFormat) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(inputFormat, Locale.ENGLISH);
        Date date1 = null;
        try {
            date1 = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat simpleOutputFormat = new SimpleDateFormat(outputFormat, Locale.ENGLISH);
        if (date1 != null) {
            return simpleOutputFormat.format(date1);
        }

        return null;
    }

    public static String getConvertedTime(String createdTime, String outFormat) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.ENGLISH);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String targetdatevalue = "";
        try {
            Date startDate = simpleDateFormat.parse(createdTime);
            SimpleDateFormat targetFormat = new SimpleDateFormat(outFormat, Locale.ENGLISH);
            targetFormat.setTimeZone(Calendar.getInstance().getTimeZone());
            targetdatevalue = targetFormat.format(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetdatevalue;

    }

    public static String getFormatedString(String createdTime, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.ENGLISH);
        String targetdatevalue = "";
        try {
            Date startDate = simpleDateFormat.parse(createdTime);
            SimpleDateFormat targetFormat = new SimpleDateFormat(format, Locale.ENGLISH);
            targetdatevalue = targetFormat.format(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetdatevalue;

    }

    public static String getFormatedString1(String createdTime, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss.sss'Z'", Locale.ENGLISH);
        String targetdatevalue = "";
        try {
            Date startDate = simpleDateFormat.parse(createdTime);
            SimpleDateFormat targetFormat = new SimpleDateFormat(format, Locale.ENGLISH);
            targetdatevalue = targetFormat.format(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return targetdatevalue;

    }

    public static float convertDpToPixel(float dp, Context context) {
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }


    public static String getBonusTime(String createdTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.ENGLISH);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        String diff = "";
        try {
            Date startDate = simpleDateFormat.parse(createdTime);
            Date currentDate = getCurrentTime();

            diff = printDifference(currentDate, startDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return diff;

    }

    public static Date getCurrentTime() {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'", Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        final String utcTime = sdf.format(new Date());

        try {
            return sdf.parse(utcTime);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }

    }


    public static String printDifference(Date startDate, Date endDate) {

        if (startDate == null || endDate == null) {
            return "";
        }
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        if (different <= 0) {
            return "";
        }
        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        if (elapsedHours == 0) {
            if (elapsedMinutes == 0) {
                if (elapsedSeconds == 0) {
                    return " ";
                } else {
                    return elapsedSeconds + " Sec";
                }
            } else {
                return elapsedMinutes + "Min " + elapsedSeconds + " Sec";
            }
        } else {
            return elapsedHours + "hrs " + elapsedMinutes + "Min " + elapsedSeconds + " Sec";
        }

    }

}
