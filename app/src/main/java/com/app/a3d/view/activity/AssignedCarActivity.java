package com.app.a3d.view.activity;

import android.app.Dialog;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ActivityAssignedCarBinding;
import com.app.a3d.interfaces.OnClickListener;
import com.app.a3d.model.CommonResponse;
import com.app.a3d.model.Damages;
import com.app.a3d.model.DashboardData;
import com.app.a3d.model.DashboardResponse;
import com.app.a3d.model.MaintanaceList;
import com.app.a3d.model.MaintenanceListData;
import com.app.a3d.model.MaintenanceListResponse;
import com.app.a3d.model.ProfileData;
import com.app.a3d.model.ProfileResponse;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.utils.Utils;
import com.app.a3d.view.adapter.DamagesAdapter;
import com.app.a3d.view.adapter.MaintenanceListAdapter;
import com.app.a3d.viewmodel.CommonViewModel;

import org.json.JSONArray;

import java.util.ArrayList;

public class AssignedCarActivity extends BaseActivity {

    CommonViewModel commonViewModel;
    DashboardData profileData;
    ActivityAssignedCarBinding binding;
    DamagesAdapter adapter;
    Dialog maintenanceDialog;
    Dialog returnCarDialog;

    String selectedMaintenanceId = "";
    ArrayList<MaintenanceListData> maintenaceList = new ArrayList<>();
    ArrayList<Damages> damages = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_assigned_car);
        commonViewModel = new ViewModelProvider(this).get(CommonViewModel.class);
        initAdapter();
        initListeners();

    }

    private void initListeners() {

        binding.dashBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AssignedCarActivity.this, DashboardActivity.class);
                intent.putExtra("current", 0);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });


        binding.assignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AssignedCarActivity.this, DashboardActivity.class);
                intent.putExtra("current", 1);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        binding.imageView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.view12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profileData != null)
                    getMaintenanceList();
            }
        });

        binding.view13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profileData != null)
                    showReturnCar();
            }


        });

    }


    private void getMaintenanceList() {
        DialogUtils.showLoader(this);
        commonViewModel.getMaintenanceList(this).observe(this, new Observer<MaintenanceListResponse>() {
            @Override
            public void onChanged(MaintenanceListResponse maintenanceListResponse) {
                DialogUtils.dismissLoader();
                if (maintenanceListResponse.getError()) {
                    Utils.showSnack(binding.parentView, maintenanceListResponse.getMessage());
                } else {
                    if (maintenanceListResponse.getData().getList().size() != 0) {
                        maintenaceList = maintenanceListResponse.getData().getList();
                        showMaintenanceDialog();
                    }
                }
            }
        });
    }

    private void showMaintenanceDialog() {

        maintenanceDialog = DialogUtils.getMaintenanceDialog(this);
        selectedMaintenanceId = "";

        CardView cardView3 = maintenanceDialog.findViewById(R.id.cardView3);
        TextView mileage = maintenanceDialog.findViewById(R.id.mileage);
        EditText amount = maintenanceDialog.findViewById(R.id.amount);
        EditText currentMileage = maintenanceDialog.findViewById(R.id.currentMileage);
        EditText message = maintenanceDialog.findViewById(R.id.message);
        CardView save = maintenanceDialog.findViewById(R.id.save);
        ConstraintLayout dialogParent = maintenanceDialog.findViewById(R.id.dialogParent);
        RecyclerView maintananceList = maintenanceDialog.findViewById(R.id.maintananceList);
        ImageView close = maintenanceDialog.findViewById(R.id.close);


        MaintenanceListAdapter adapter = new MaintenanceListAdapter(this, maintenaceList, new OnClickListener() {
            @Override
            public void onClick(int position) {

                selectedMaintenanceId = maintenaceList.get(position).getId();
                mileage.setText(maintenaceList.get(position).getNameList());
                maintananceList.setVisibility(View.GONE);

            }
        });
        maintananceList.setLayoutManager(new LinearLayoutManager(this));
        maintananceList.setAdapter(adapter);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                maintenanceDialog.dismiss();
            }
        });

        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (maintananceList.getVisibility() == View.VISIBLE) {
                    maintananceList.setVisibility(View.GONE);
                } else {
                    maintananceList.setVisibility(View.VISIBLE);
                    adapter.setChecked(selectedMaintenanceId);
                }
            }
        });

        save.setOnClickListener(v -> {
            if (selectedMaintenanceId.equalsIgnoreCase("")) {
                Utils.showSnack(dialogParent, getString(R.string.maintenance_type));
            } else if (amount.getText().toString().trim().equalsIgnoreCase("")) {
                Utils.showSnack(dialogParent, getString(R.string.enter_amount));
            } else if (currentMileage.getText().toString().trim().equalsIgnoreCase("")) {
                Utils.showSnack(dialogParent, getString(R.string.enter_current_mileage));
            } else if (message.getText().toString().trim().equalsIgnoreCase("")) {
                Utils.showSnack(dialogParent, getString(R.string.add_message));
            } else {
                DialogUtils.showLoader(AssignedCarActivity.this);
                commonViewModel.addMaintenance(AssignedCarActivity.this, profileData.getUcarId(), selectedMaintenanceId, amount.getText().toString().trim(),
                        currentMileage.getText().toString().trim(),
                        message.getText().toString().trim()).observe(AssignedCarActivity.this, new Observer<CommonResponse>() {
                    @Override
                    public void onChanged(CommonResponse commonResponse) {
                        DialogUtils.dismissLoader();
                        if (commonResponse.getError()) {
                            Utils.showSnack(dialogParent, commonResponse.getMessage());
                        } else {
                            maintenanceDialog.dismiss();
                            onResume();
                        }
                    }
                });
            }
        });

        maintenanceDialog.show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getProfileDetails();
    }

   /* @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(AssignedCarActivity.this, AssignedCarActivity.class);
        startActivity(intent);
        finish();
    }*/

    private void getProfileDetails() {

        DialogUtils.showLoader(this);
        commonViewModel.dashBoardDetails(this).observe(this, new Observer<DashboardResponse>() {
            @Override
            public void onChanged(DashboardResponse profileResponse) {
                DialogUtils.dismissLoader();
                if (profileResponse.getError()) {
                    Utils.showSnack(binding.parentView, profileResponse.getMessage());
                } else {
                    setData(profileResponse.getData().getProfile());
                }
            }

        });

    }

    private void setData(DashboardData profile) {

        if (profile == null) return;

        profileData = profile;

        binding.carId.setText(profile.getCarID());
        binding.modelNumber.setText(profile.getCarModel());
        binding.licencePlateNumber.setText(profile.getLicenseNumber());
        binding.currentMillage.setText(profile.getCurrentMileage());

        if (profile.getLastDateOilChange() != null)
            binding.dateOilChange.setText(Utils.getFormatedString1(String.valueOf(profile.getLastDateOilChange()+"T00:00:00.000Z"), "dd MMM,yyyy"));

        if (profile.getLastDateGasRefill() != null)
            binding.dateGasRefill.setText(Utils.getFormatedString1(String.valueOf(profile.getLastDateGasRefill()+"T00:00:00.000Z"), "dd MMM,yyyy"));

        if (profile.getExpirationDate() != null)
            binding.expirationDate.setText(Utils.getFormatedString(profile.getExpirationDate(), "dd MMM,yyyy"));

        adapter.setList(profile.getDamage());

    }

    private void initAdapter() {

        adapter = new DamagesAdapter(this, new ArrayList<>());
        binding.damageList.setLayoutManager(new LinearLayoutManager(this));
        binding.damageList.setAdapter(adapter);

    }


    private void showReturnCar() {

        damages = profileData.getDamage();

        returnCarDialog = DialogUtils.getReturnCarDialog(this);
        ImageView close = returnCarDialog.findViewById(R.id.close);
        EditText mileage = returnCarDialog.findViewById(R.id.mileage);
        RecyclerView damageList = returnCarDialog.findViewById(R.id.damageList);
        EditText damage = returnCarDialog.findViewById(R.id.damage);
        TextView add = returnCarDialog.findViewById(R.id.add);
        CardView save = returnCarDialog.findViewById(R.id.save);
        ConstraintLayout dialogParent = returnCarDialog.findViewById(R.id.dialogParent);

        DamagesAdapter damageAdapters = new DamagesAdapter(this, damages);
        damageList.setLayoutManager(new LinearLayoutManager(this));
        damageList.setAdapter(damageAdapters);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnCarDialog.dismiss();
                getProfileDetails();
            }
        });

        add.setOnClickListener(v -> {
            if (damage.getText().toString().trim().length() != 0) {
                damageAdapters.addDamage(damage.getText().toString().trim());
                damage.setText("");
                damageList.smoothScrollToPosition(damages.size() - 1);
            } else {
                Utils.showSnack(dialogParent, getString(R.string.enter_damage));
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mileage.getText().toString().trim().length() == 0) {
                    Utils.showSnack(dialogParent, getString(R.string.enter_current_mileage));
                } else {
                    DialogUtils.showLoader(AssignedCarActivity.this);
                    commonViewModel.returnCar(AssignedCarActivity.this, mileage.getText().toString().trim(),
                            profileData.getUcarId(), getNewDamages(damages)).observe(AssignedCarActivity.this, commonResponse -> {
                        DialogUtils.dismissLoader();
                        if (commonResponse.getError()) {
                            Utils.showSnack(dialogParent, commonResponse.getMessage());
                        } else {
                            getProfileDetails();
                            returnCarDialog.dismiss();
                        }
                    });
                }
            }
        });

        returnCarDialog.show();

    }

    private String getNewDamages(ArrayList<Damages> damages) {

        JSONArray array = new JSONArray();
        for (int i = 0; i < damages.size(); i++) {
            if (damages.get(i).isAddedNow()) {
                array.put(damages.get(i).getDamageReason());
            }
        }
        return array.toString();
    }

}
