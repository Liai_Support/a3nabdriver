package com.app.a3d.view.activity;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.LocaleList;
import android.util.DisplayMetrics;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;


import com.app.a3d.utils.SharedHelper;

import java.util.Locale;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        setPhoneLanguage();

    }

    private void setPhoneLanguage() {


        Resources res = getResources();
        Configuration conf = res.getConfiguration();
        Locale locale;
        if (new SharedHelper(this).getSelectedLanguage().length() > 0) {
            locale = new Locale(new SharedHelper(this).getSelectedLanguage().toLowerCase());
        } else {
            locale = new Locale("en");
        }

        Locale.setDefault(locale);
        conf.setLocale(locale);
        getApplicationContext().createConfigurationContext(conf);


        DisplayMetrics dm = res.getDisplayMetrics();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            conf.setLocales(new LocaleList(locale));
        } else {
            conf.locale = locale;
        }
        res.updateConfiguration(conf, dm);
    }

}
