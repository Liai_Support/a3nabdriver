package com.app.a3d.view.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3d.R;
import com.app.a3d.databinding.ActivityDashboardBinding;
import com.app.a3d.utils.SharedHelper;
import com.app.a3d.utils.Utils;
import com.app.a3d.view.adapter.HomeFragmentAdapter;
import com.app.a3d.view.fragment.AssignmentFragment;
import com.app.a3d.view.fragment.HomeFragment;
import com.app.a3d.viewmodel.CommonViewModel;

import java.util.ArrayList;

public class DashboardActivity extends BaseActivity {

    AssignmentFragment assignmentFragment = new AssignmentFragment();
    HomeFragment homeFragment = new HomeFragment();

    ArrayList<Fragment> listOfFragment = new ArrayList<>();
    HomeFragmentAdapter adapter;

    ActivityDashboardBinding binding;
    public String currentAssignmentRouteId = "";
    CommonViewModel commonViewModel;
    SharedHelper sharedHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dashboard);
        commonViewModel = new ViewModelProvider(this).get(CommonViewModel.class);
        sharedHelper = new SharedHelper(this);
        initView();
        initAdapter();
        updateDeviceToken();
        getIntentValues();
        showAllowSoundAlert();
    }

    private void getIntentValues() {

        binding.container.setCurrentItem(getIntent().getExtras().getInt("current", 0));

    }

    private void updateDeviceToken() {
        commonViewModel.updateDeviceToken(this);
    }
    private void showAllowSoundAlert() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            if (!sharedHelper.isShowSoundDialog())
                new AlertDialog.Builder(this)
                        .setTitle(R.string.enable_sound)
                        .setMessage(R.string.enable_sound_content)
                        .setPositiveButton(R.string.allow_sound, (dialogInterface, i) -> {

                            Intent settingsIntent = null;
                            settingsIntent = new Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    .putExtra(Settings.EXTRA_APP_PACKAGE, getPackageName())
                                    .putExtra(Settings.EXTRA_CHANNEL_ID, "Notification_Sound");

                            startActivity(settingsIntent);

                            sharedHelper.setShowSoundDialog(true);
                        })
                        .setNegativeButton(R.string.skip, (dialogInterface, i) -> {
                            sharedHelper.setShowSoundDialog(true);
                            dialogInterface.dismiss();
                        }).show();
        }
    }


    private void initView() {
        listOfFragment.add(homeFragment);
        listOfFragment.add(assignmentFragment);

    }


    private void initAdapter() {

        adapter = new HomeFragmentAdapter(getSupportFragmentManager(), listOfFragment,getLifecycle());
        binding.container.setUserInputEnabled(false);
        binding.container.setAdapter(adapter);

        binding.dashBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.container.setCurrentItem(0);
            }
        });


        binding.assignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.container.setCurrentItem(1);
            }
        });

        binding.currentAssignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!currentAssignmentRouteId.equalsIgnoreCase(""))
                    startActivity(new Intent(DashboardActivity.this, CurrentAssignmentActivity.class)
                            .putExtra("routeId", currentAssignmentRouteId));
                else
                    Utils.showSnack(binding.parent, getString(R.string.no_assignment));
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (homeFragment != null) {
            homeFragment.onFragmentResult(requestCode, resultCode, data);
        }
    }

}
