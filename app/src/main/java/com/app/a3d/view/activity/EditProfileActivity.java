package com.app.a3d.view.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3d.R;
import com.app.a3d.databinding.ActivityEditProfileBinding;
import com.app.a3d.model.CommonResponse;
import com.app.a3d.model.ProfileData;
import com.app.a3d.model.ProfileResponse;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.utils.SharedHelper;
import com.app.a3d.utils.Utils;
import com.app.a3d.viewmodel.CommonViewModel;

import java.util.Calendar;

public class EditProfileActivity extends BaseActivity {

    ActivityEditProfileBinding binding;
    CommonViewModel commonViewModel;
    String mobileNumber = "";
    SharedHelper sharedHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile);
        commonViewModel = new ViewModelProvider(this).get(CommonViewModel.class);
        sharedHelper = new SharedHelper(this);
        initListener();
    }

    private void initListener() {

        binding.dashBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EditProfileActivity.this, DashboardActivity.class);
                intent.putExtra("current", 0);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });


        binding.assignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditProfileActivity.this, DashboardActivity.class);
                intent.putExtra("current", 1);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        binding.dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCalenderDialog();
            }
        });

        binding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidInputs()) {
                    DialogUtils.showLoader(EditProfileActivity.this);
                    commonViewModel.updateProfile(EditProfileActivity.this,
                            binding.firstName.getText().toString().trim(),
                            binding.lastName.getText().toString().trim(),
                            binding.emailAddress.getText().toString().trim(),
                            mobileNumber,
                            binding.male.isChecked(),
                            binding.dob.getText().toString().trim()).observe(EditProfileActivity.this, new Observer<CommonResponse>() {
                        @Override
                        public void onChanged(CommonResponse commonResponse) {
                            DialogUtils.dismissLoader();
                            if (commonResponse.getError()) {
                                Utils.showSnack(binding.parentView, commonResponse.getMessage());
                            } else {
                                sharedHelper.setFirstName(binding.firstName.getText().toString().trim());
                                sharedHelper.setLastName(binding.lastName.getText().toString().trim());
                                finish();
                            }
                        }
                    });
                }
            }
        });
    }

    private boolean isValidInputs() {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (binding.firstName.getText().toString().trim().length() == 0) {
            Utils.showSnack(binding.parentView, getString(R.string.firt_name_empty));
            return false;
        } else if (binding.lastName.getText().toString().trim().length() == 0) {
            Utils.showSnack(binding.parentView, getString(R.string.last_name_empty));
            return false;
        } else if (binding.emailAddress.getText().toString().trim().length() == 0 || (!binding.emailAddress.getText().toString().matches(emailPattern))) {
            Utils.showSnack(binding.parentView, getString(R.string.inavlid_email));
            return false;
        } else if ((!binding.male.isChecked()) && (!binding.female.isChecked())) {
            Utils.showSnack(binding.parentView, getString(R.string.select_gender));
            return false;
        } else if (binding.dob.getText().toString().trim().length() == 0) {
            Utils.showSnack(binding.parentView, getString(R.string.select_dob));
            return false;
        }
        return true;
    }

    private void showCalenderDialog() {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        binding.dob.setText(year + "-" + Utils.formatedValues(monthOfYear + 1) + "-" + Utils.formatedValues(dayOfMonth));

                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getProfileDetails();
    }

    private void getProfileDetails() {

        commonViewModel.getProfileDetails(this).observe(this, new Observer<ProfileResponse>() {
            @Override
            public void onChanged(ProfileResponse profileResponse) {
                if (profileResponse.getError()) {
                    Utils.showSnack(binding.parentView, profileResponse.getMessage());
                } else {
                    setData(profileResponse.getData().getProfile());
                }
            }

        });

    }

    private void setData(ProfileData profile) {

        if (profile == null)
            return;

        mobileNumber = profile.getMobileNumber();
        binding.firstName.setText(profile.getFirstName());
        binding.lastName.setText(profile.getLastName());
        binding.emailAddress.setText(profile.getEmail());
        binding.phoneNumber.setText("+" + profile.getCountryCode() + " " + profile.getMobileNumber());
        binding.phoneNumber.setEnabled(false);
        binding.dob.setText(profile.getDob());

        if (profile.getGender().equalsIgnoreCase("male")) {
            binding.male.setChecked(true);
            binding.female.setChecked(false);
        } else if (profile.getGender().equalsIgnoreCase("female")) {
            binding.male.setChecked(false);
            binding.female.setChecked(true);
        } else {
            binding.male.setChecked(false);
            binding.female.setChecked(false);
        }
    }


}
