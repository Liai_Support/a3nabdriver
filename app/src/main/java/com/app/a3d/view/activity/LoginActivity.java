package com.app.a3d.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3d.R;
import com.app.a3d.databinding.ActivityLoginBinding;
import com.app.a3d.model.FireBaseVerificationResponse;
import com.app.a3d.model.LoginDetails;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.utils.SessionManager;
import com.app.a3d.utils.SharedHelper;
import com.app.a3d.utils.Utils;
import com.app.a3d.view.fragment.OTPVerificationFragment;
import com.app.a3d.viewmodel.SignUpViewModel;
import com.google.firebase.iid.FirebaseInstanceId;


public class LoginActivity extends BaseActivity {


    SignUpViewModel viewModel;
    ActivityLoginBinding activityLoginBinding;
    OTPVerificationFragment otpVerificationFragment;
    SharedHelper sharedHelper;
    Handler handler = new Handler();
    Runnable runnable;
    public String tuserid = "";
    public String tfirstname = "";
    public String tlastname = "";
    public String tmailid = "";
    public String tlatitude = "";
    public String tlongitude = "";
    public String tcountrycode = "";
    public String tmobilenumber = "";
    public String tprofilepic = "";
    public String tauthtoken = "";
    public Boolean tloggedin = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        viewModel = new ViewModelProvider(this).get(SignUpViewModel.class);
        sharedHelper = new SharedHelper(this);
        initListener();
        checkForFcmToken();
    }

    private void initListener() {


        activityLoginBinding.imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        activityLoginBinding.registerLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
                intent.putExtra("current", 0);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });
        activityLoginBinding.next.setOnClickListener((view) -> {

//            otpVerified();
            if (activityLoginBinding.mobileNumber.getText().toString().length() >= 6) {

                SessionManager.getInstance().setCountryCode(activityLoginBinding.countryCodePicker.getSelectedCountryCode());
                SessionManager.getInstance().setMobileNumber(activityLoginBinding.mobileNumber.getText().toString());
                otpVerified();

             /*   DialogUtils.showLoader(this);
                viewModel.requestVerificationCode(
                        this,
                        "+" + activityLoginBinding.countryCodePicker.getSelectedCountryCode() +
                                activityLoginBinding.mobileNumber.getText().toString()
                ).observe(this, new Observer<FireBaseVerificationResponse>() {
                    @Override
                    public void onChanged(FireBaseVerificationResponse fireBaseVerificationResponse) {

                        if (fireBaseVerificationResponse.getError().equalsIgnoreCase("true")) {
                            Log.d("dscxc","xcxc");
                            DialogUtils.dismissLoader();
                            Utils.showSnack(activityLoginBinding.parentLayout, fireBaseVerificationResponse.getMessage());

                          *//*  //hari
                            SessionManager.getInstance().setCountryCode(activityLoginBinding.countryCodePicker.getSelectedCountryCode());
                            SessionManager.getInstance().setMobileNumber(activityLoginBinding.mobileNumber.getText().toString());
                            otpVerified();*//*

                        } else {
                            SessionManager.getInstance().setCountryCode(activityLoginBinding.countryCodePicker.getSelectedCountryCode());
                            SessionManager.getInstance().setMobileNumber(activityLoginBinding.mobileNumber.getText().toString());
                            Log.d("dscxcx0x","xzczxxcxc");
                            if (fireBaseVerificationResponse.getVerificationCode().equalsIgnoreCase("verified")) {
                                otpVerified();
                                Log.d("dscxcxx1","xzczxxcxc");
                            } else {
                                Log.d("dscxcxx2","xzczxxcxc");
                                navigateToVerification(fireBaseVerificationResponse.getVerificationCode());
                            }
                        }
                    }
                });*/
            } else {
                Utils.showSnack(activityLoginBinding.parentLayout, getString(R.string.enter_mobile_number));
            }

        });

        activityLoginBinding.changeLanguage.setOnClickListener(view -> {

            if (sharedHelper.getSelectedLanguage().equals("en")) {
                sharedHelper.setSelectedLanguage("ar");
            } else {
                sharedHelper.setSelectedLanguage("en");
            }
            Intent intent = new Intent(LoginActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);


        });
    }

    private void otpVerified() {
        DialogUtils.showLoader(this);
        viewModel.login(this, activityLoginBinding.countryCodePicker.getSelectedCountryCode(),
                activityLoginBinding.mobileNumber.getText().toString()).observe(this, loginResponse -> {
            DialogUtils.dismissLoader();
            if (loginResponse.getError()) {
                Utils.showSnack(findViewById(android.R.id.content), loginResponse.getMessage());
            } else {
                userExists(loginResponse.getData().getLoginDetails());

            }
        });
    }

    private void userExists(LoginDetails data) {

        if (data.getStoreExists().equalsIgnoreCase("true")) {
            DialogUtils.showLoader(this);

            tuserid = String.valueOf(data.getId());
            tfirstname = data.getFirstName();
            tlastname = data.getLastName();
            tmailid = data.getEmail();
            tcountrycode = data.getCountryCode();
            tmobilenumber = data.getMobileNumber();
            tauthtoken = data.getToken();
            tloggedin = false;

         /*   sharedHelper.setUserId(String.valueOf(data.getId()));
            sharedHelper.setFirstName(data.getFirstName());
            sharedHelper.setLastName(data.getLastName());
            sharedHelper.setEmail(data.getEmail());
            sharedHelper.setCountryCode(data.getCountryCode());
            sharedHelper.setMobileNumber(data.getMobileNumber());
            sharedHelper.setAuthToken(data.getToken());
            sharedHelper.setLoggedIn(true);*/

            navigateToVerification(""+data.getOtp());


          /*  Intent intent = new Intent(this, DashboardActivity.class);
            intent.putExtra("current", 0);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();*/

        } else {
            Utils.showSnack(findViewById(android.R.id.content), getString(R.string.user_not_exist));
        }


    }


    private void navigateToVerification(String verificationCode) {

        runnable = () -> {
            DialogUtils.dismissLoader();
            SessionManager.getInstance().setVerificationCode(verificationCode);
            otpVerificationFragment = new OTPVerificationFragment();

            Utils.navigateToFragment(
                    getSupportFragmentManager(),
                    R.id.container,
                    otpVerificationFragment
            );

            //Password verified

        };

        handler.postDelayed(runnable, 3000);


    }

    private void checkForFcmToken() {

        if (new SharedHelper(this).getFirebaseToken().equalsIgnoreCase("")) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
                String newToken = instanceIdResult.getToken();
                new SharedHelper(this).setFirebaseToken(newToken);
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
      /*  Log.d("dcbhdvd","sdcbhdbc");
        if(!SessionManager.getInstance().getVerificationCode().equals("")){
            Toast.makeText(this,""+SessionManager.getInstance().getVerificationCode(),Toast.LENGTH_SHORT).show();
        }*/
    }


}
