package com.app.a3d.view.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3d.R;
import com.app.a3d.databinding.ActivityMakeDeliveryBinding;
import com.app.a3d.model.DashboardResponse;
import com.app.a3d.model.RouteDetails;
import com.app.a3d.model.RoutesPartition;
import com.app.a3d.model.TopStore;
import com.app.a3d.model.TopUsers;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.utils.Utils;
import com.app.a3d.view.adapter.MakeDeliveryAdapter;
import com.app.a3d.viewmodel.CommonViewModel;
import com.google.gson.Gson;

import java.util.ArrayList;

public class MakeDeliveryActivity extends BaseActivity {

    RoutesPartition routeDetail = null;
    TopUsers orderDetail = null;
    String routeId = "", adminNumber = "";
    ActivityMakeDeliveryBinding binding;
    CommonViewModel commonViewModel;
    public Double amount = 0.0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_make_delivery);
        commonViewModel = new ViewModelProvider(this).get(CommonViewModel.class);
        getIntentValues();
        initListener();
        setAdapter();

    }

    private void getIntentValues() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String dataStr = bundle.getString("customerData");
            String dataStr1 = bundle.getString("orderData");
            Gson gson = new Gson();
            routeDetail = gson.fromJson(dataStr, RoutesPartition.class);
            routeId = bundle.getString("routeId");
            adminNumber = bundle.getString("adminNumber");
        }

    }

    private void initListener() {

        binding.imageView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        binding.makeDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (routeDetail != null)
                    makeDelivery();
            }
        });

    }

    private void setAdapter() {

        if (routeDetail != null) {

            ArrayList<RoutesPartition> list = new ArrayList<>();
            ArrayList<TopUsers> list1 = new ArrayList<>();
            list1.add(orderDetail);
//            Log.d("zcbzxc",""+list1.get(0).getGrandTotal());
            list.add(routeDetail);

            binding.recyclerView2.setLayoutManager(new LinearLayoutManager(this));
            binding.recyclerView2.setAdapter(new MakeDeliveryAdapter(MakeDeliveryActivity.this,this, list,list1, adminNumber));

        }

    }


    private void makeDelivery() {
        DialogUtils.showLocationNotes(this, (isHelpFul, notes) -> {


            DialogUtils.showLoader(this);
            commonViewModel.makeDelivery(this,amount,routeDetail.getUserId(), routeId, routeDetail.getOrderId(), notes,isHelpFul).observe(this, commonResponse -> {
                DialogUtils.dismissLoader();
                if (commonResponse.getError()) {
                    Utils.showSnack(binding.parent, commonResponse.getMessage());
                } else {
                    finish();
                }
            });

        });
    }
}


