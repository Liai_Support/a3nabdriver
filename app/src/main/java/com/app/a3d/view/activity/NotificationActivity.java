package com.app.a3d.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3d.R;
import com.app.a3d.databinding.ActivityNotificationBinding;
import com.app.a3d.model.NotificationData;
import com.app.a3d.model.NotificationResponseModel;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.view.adapter.NotificationAdapter;
import com.app.a3d.viewmodel.CommonViewModel;

public class NotificationActivity extends BaseActivity {

    CommonViewModel commonViewModel;
    ActivityNotificationBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        commonViewModel = new ViewModelProvider(this).get(CommonViewModel.class);

        getList();

        initListener();

        binding.imageView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initListener() {

        binding.dashBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(NotificationActivity.this, DashboardActivity.class);
                intent.putExtra("current", 0);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });


        binding.assignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NotificationActivity.this, DashboardActivity.class);
                intent.putExtra("current", 1);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        binding.currentAssignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (!currentAssignmentRouteId.equalsIgnoreCase(""))
//                    startActivity(new Intent(DashboardActivity.this, CurrentAssignmentActivity.class)
//                            .putExtra("routeId", currentAssignmentRouteId));
//                else
//                    Utils.showSnack(binding.parent, getString(R.string.no_assignment));
            }
        });

    }


    private void getList() {

        DialogUtils.showLoader(this);
        commonViewModel.getNotificationList(this).observe(this, new Observer<NotificationResponseModel>() {
            @Override
            public void onChanged(NotificationResponseModel notificationResponseModel) {
                DialogUtils.dismissLoader();
                if (notificationResponseModel.getError()) {

                } else {
                    setAdapter(notificationResponseModel.getData());
                }
            }
        });

    }

    private void setAdapter(NotificationData data) {

        binding.list.setLayoutManager(new LinearLayoutManager(this));
        binding.list.setAdapter(new NotificationAdapter(this, data.getNotification()));

    }
}
