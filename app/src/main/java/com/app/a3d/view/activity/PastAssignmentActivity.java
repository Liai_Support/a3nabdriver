package com.app.a3d.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3d.R;
import com.app.a3d.databinding.ActivityAcceptAssignedCarBinding;
import com.app.a3d.databinding.ActivityPastAssignmentBinding;
import com.app.a3d.model.CarDetails;
import com.app.a3d.model.CommonResponse;
import com.app.a3d.model.DashboardResponse;
import com.app.a3d.model.GetRouteResponse;
import com.app.a3d.model.NewCar;
import com.app.a3d.model.ViewRouteResponse;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.utils.Utils;
import com.app.a3d.view.adapter.DamagesAdapter;
import com.app.a3d.view.adapter.PastAssignmentAdapter;
import com.app.a3d.view.adapter.PastRoutesAdapter;
import com.app.a3d.viewmodel.CommonViewModel;

import java.util.ArrayList;

public class PastAssignmentActivity extends BaseActivity {

    CommonViewModel commonViewModel;
    ActivityPastAssignmentBinding binding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_past_assignment);
        commonViewModel = new ViewModelProvider(this).get(CommonViewModel.class);
        initListeners();
        getIntentValues();

    }

    private void getIntentValues() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            getRoutes(bundle.getString("routeId", ""));
        }

    }

    private void initListeners() {

        binding.imageView.setOnClickListener(v -> startActivity(new Intent(this, NotificationActivity.class)));
        binding.imageView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.dashBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(PastAssignmentActivity.this, DashboardActivity.class);
                intent.putExtra("current", 0);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });


        binding.assignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PastAssignmentActivity.this, DashboardActivity.class);
                intent.putExtra("current", 1);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

    }


    private void getRoutes(String id) {

        DialogUtils.showLoader(this);
        commonViewModel.getViewRoutes(this, id).observe(this, getRouteResponse -> {
            DialogUtils.dismissLoader();
            if (getRouteResponse.getError()) {
                Utils.showSnack(binding.parent, getRouteResponse.getMessage());
            } else {

                setDetails(getRouteResponse);


            }
        });

    }

    private void setDetails(ViewRouteResponse getRouteResponse) {

        binding.orders.setText(getRouteResponse.getData().getRouteDetail().getOrders());
        binding.pickupLocation.setText(getRouteResponse.getData().getRouteDetail().getPickupCount());
        binding.dropCount.setText(getRouteResponse.getData().getRouteDetail().getDropCount());

        binding.assignedDate.setText(getString(R.string.date) + Utils.getConvertedTime(getRouteResponse.getData().getRouteDetail().getAssignDate(), "dd MMM,yyyy"));
        binding.assignedTime.setText(getString(R.string.time) + Utils.getConvertedTime(getRouteResponse.getData().getRouteDetail().getAssignDate(), "hh:mm a"));


        binding.routeDetails.setLayoutManager(new LinearLayoutManager(this));
        binding.routeDetails.setAdapter(new PastRoutesAdapter(getRouteResponse.getData().getRoutes(),this));
    }


}
