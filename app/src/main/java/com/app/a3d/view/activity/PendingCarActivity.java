package com.app.a3d.view.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ActivityAcceptAssignedCarBinding;
import com.app.a3d.databinding.ActivityAssignedCarBinding;
import com.app.a3d.interfaces.OnClickListener;
import com.app.a3d.model.CarDetails;
import com.app.a3d.model.CommonResponse;
import com.app.a3d.model.Damages;
import com.app.a3d.model.DashboardData;
import com.app.a3d.model.DashboardResponse;
import com.app.a3d.model.MaintenanceListData;
import com.app.a3d.model.MaintenanceListResponse;
import com.app.a3d.model.NewCar;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.utils.Utils;
import com.app.a3d.view.adapter.DamagesAdapter;
import com.app.a3d.view.adapter.MaintenanceListAdapter;
import com.app.a3d.viewmodel.CommonViewModel;

import org.json.JSONArray;

import java.util.ArrayList;

public class PendingCarActivity extends BaseActivity {

    CommonViewModel commonViewModel;
    CarDetails profileData;
    ActivityAcceptAssignedCarBinding binding;
    DamagesAdapter adapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_accept_assigned_car);
        commonViewModel = new ViewModelProvider(this).get(CommonViewModel.class);
        initAdapter();
        initListeners();

    }

    private void initListeners() {


        binding.dashBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(PendingCarActivity.this, DashboardActivity.class);
                intent.putExtra("current", 0);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });


        binding.assignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PendingCarActivity.this, DashboardActivity.class);
                intent.putExtra("current", 1);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        binding.imageView11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.view12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.showLoader(PendingCarActivity.this);

                commonViewModel.acceptCar(PendingCarActivity.this).observe(PendingCarActivity.this, new Observer<CommonResponse>() {
                    @Override
                    public void onChanged(CommonResponse commonResponse) {
                        DialogUtils.dismissLoader();
                        if (commonResponse.getError()) {
                            Utils.showSnack(binding.parentView, commonResponse.getMessage());
                        } else {
                            finish();
                        }
                    }
                });
            }
        });


    }


    @Override
    protected void onResume() {
        super.onResume();
        getProfileDetails();
    }


    private void getProfileDetails() {

        DialogUtils.showLoader(this);
        commonViewModel.dashBoardDetails(this).observe(this, new Observer<DashboardResponse>() {
            @Override
            public void onChanged(DashboardResponse profileResponse) {
                DialogUtils.dismissLoader();
                if (profileResponse.getError()) {
                    Utils.showSnack(binding.parentView, profileResponse.getMessage());
                } else {
                    setData(profileResponse.getData().getNewCar());
                }
            }

        });

    }

    private void setData(NewCar profile) {

        if (profile == null) return;

        profileData = profile.getCar().get(0);

        binding.carId.setText(profileData.getCarID());
        binding.modelNumber.setText(profileData.getCarModel());
        binding.licencePlateNumber.setText(profileData.getLicenseNumber());
        binding.currentMillage.setText(profileData.getCurrentMileage());

        if (profileData.getLastDateOilChange() != null)
            binding.dateOilChange.setText(Utils.getFormatedString1(String.valueOf(profileData.getLastDateOilChange())+"T00:00:00.000Z", "dd MMM,yyyy"));

        if (profileData.getLastDateGasRefill() != null)
            binding.dateGasRefill.setText(Utils.getFormatedString1(String.valueOf(profileData.getLastDateOilChange()+"T00:00:00.000Z"), "dd MMM,yyyy"));

        if (profileData.getExpirationDate() != null)
            binding.expirationDate.setText(Utils.getFormatedString(profileData.getExpirationDate(), "dd MMM,yyyy"));

        adapter.setList(profile.getDamage());

    }

    private void initAdapter() {

        adapter = new DamagesAdapter(this, new ArrayList<>());
        binding.damageList.setLayoutManager(new LinearLayoutManager(this));
        binding.damageList.setAdapter(adapter);

    }


}
