package com.app.a3d.view.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3d.R;
import com.app.a3d.databinding.ActivityProfileBinding;
import com.app.a3d.model.CommonResponse;
import com.app.a3d.model.ProfileData;
import com.app.a3d.model.ProfileResponse;
import com.app.a3d.utils.Constants;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.utils.ImagePicker;
import com.app.a3d.utils.SharedHelper;
import com.app.a3d.utils.Utils;
import com.app.a3d.viewmodel.AmazonViewModel;
import com.app.a3d.viewmodel.CommonViewModel;

import java.io.File;

public class ProfileActivity extends BaseActivity {

    ActivityProfileBinding binding;
    CommonViewModel commonViewModel;
    SharedHelper sharedHelper;


    String doc1 = "", doc2 = "", licence = "";
    String imageUploadpath = "";
    AmazonViewModel amazonViewModel;
    String currentImageType = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        commonViewModel = new ViewModelProvider(this).get(CommonViewModel.class);
        amazonViewModel = new ViewModelProvider(this).get(AmazonViewModel.class);
        sharedHelper = new SharedHelper(this);

        initListener();
    }


    private void initListener() {

        binding.dashBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(ProfileActivity.this, DashboardActivity.class);
                intent.putExtra("current", 0);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });


        binding.assignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, DashboardActivity.class);
                intent.putExtra("current", 1);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });


        binding.currentAssignment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!sharedHelper.getcurrentAssignmentRouteId().equalsIgnoreCase(""))
                    startActivity(new Intent(ProfileActivity.this, CurrentAssignmentActivity.class)
                            .putExtra("routeId", sharedHelper.getcurrentAssignmentRouteId()));
                else
                    Utils.showSnack(binding.parentView, getString(R.string.no_assignment));
            }
        });

        binding.view12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean english = false;
                if (sharedHelper.getSelectedLanguage().equals("en")) {
                    english = true;
                } else {
                    english = false;
                }
                sharedHelper.clearValues();
                if (english == true) {
                    sharedHelper.setSelectedLanguage("en");
                } else {
                    sharedHelper.setSelectedLanguage("ar");
                }
                Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });

        binding.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileActivity.this, EditProfileActivity.class));

            }
        });
        binding.driverLicence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (licence.equalsIgnoreCase("")) {
                    currentImageType = "driverLicence";
                    checkExternalPermission();
                } else {
                    DialogUtils.showImage(ProfileActivity.this, licence);
                }
            }
        });
        binding.uploadlicence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentImageType = "driverLicence";
                checkExternalPermission();
            }
        });
        binding.docA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (doc1.equalsIgnoreCase("")) {
                    currentImageType = "documentA";
                    checkExternalPermission();
                } else {
                    DialogUtils.showImage(ProfileActivity.this, doc1);
                }
            }
        });
        binding.uploadDoc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                currentImageType = "documentA";
                checkExternalPermission();

            }
        });
        binding.docB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (doc2.equalsIgnoreCase("")) {
                    currentImageType = "documentB";
                    checkExternalPermission();
                } else {
                    DialogUtils.showImage(ProfileActivity.this, doc2);
                }
            }
        });

        binding.uploadDoc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentImageType = "documentB";
                checkExternalPermission();

            }
        });

        binding.lang.setOnClickListener(view -> {

            if (sharedHelper.getSelectedLanguage().equals("en")) {
                sharedHelper.setSelectedLanguage("ar");
            } else {
                sharedHelper.setSelectedLanguage("en");
            }
            Intent intent = new Intent(ProfileActivity.this, ProfileActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.putExtra("current", 0);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        getProfileDetails();
        binding.name.setText(sharedHelper.getFirstName() + " " + sharedHelper.getLastName());
        binding.lang.setText(getString(R.string.lang));
    }


    private void openPickerDialog() {
        Intent chooseImageIntent = ImagePicker.getPickImageIntent(ProfileActivity.this);
        startActivityForResult(chooseImageIntent, Constants.IntentPermissionCode.IMAGE_PICKER);
    }

    private void checkExternalPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                openPickerDialog();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}
                        , Constants.IntentPermissionCode.EXTERNAL_STORAGE_PERMISSION);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.IntentPermissionCode.EXTERNAL_STORAGE_PERMISSION)
            if (grantResults.length >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                openPickerDialog();
            }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            imageUploadpath = ImagePicker.getImagePath(ProfileActivity.this, resultCode, data);
            if (imageUploadpath != null) {
//                        BindingAdapter.loadImage(binding.userImage, imageUploadpath, getResources().getDrawable(R.drawable.profile_placeholder));
                uploadImage();
            } else {
                Utils.showSnack(binding.parentView, getResources().getString(R.string.unable_to_select_image));
            }
        }
    }


    private void uploadImage() {

        DialogUtils.showLoader(ProfileActivity.this);
        amazonViewModel.uploadImage(ProfileActivity.this, new File(imageUploadpath)).observe(this, new Observer<CommonResponse>() {
            @Override
            public void onChanged(CommonResponse commonResponse) {
                if (commonResponse.getError()) {
                    DialogUtils.dismissLoader();
                    Utils.showSnack(binding.parentView, commonResponse.getMessage());
                } else {
                    commonViewModel.uploadDocument(ProfileActivity.this, commonResponse.getMessage(), currentImageType).observe(ProfileActivity.this, new Observer<CommonResponse>() {
                        @Override
                        public void onChanged(CommonResponse commonResponse) {
                            DialogUtils.dismissLoader();
                            if (commonResponse.getError()) {
                                Utils.showSnack(binding.parentView, commonResponse.getMessage());
                            } else {
                                Utils.showSnack(binding.parentView, getString(R.string.upload_success));

                                if (currentImageType.equalsIgnoreCase("driverLicence")) {
                                    licence = commonResponse.getMessage();
                                } else if (currentImageType.equalsIgnoreCase("documentA")) {
                                    doc1 = commonResponse.getMessage();
                                } else if (currentImageType.equalsIgnoreCase("documentB")) {
                                    doc2 = commonResponse.getMessage();
                                }
                                getProfileDetails();
                            }
                        }
                    });
                }
            }
        });
    }

    private void getProfileDetails() {

        commonViewModel.getProfileDetails(ProfileActivity.this).observe(this, new Observer<ProfileResponse>() {
            @Override
            public void onChanged(ProfileResponse profileResponse) {
                if (profileResponse.getError()) {
                    Utils.showSnack(binding.parentView, profileResponse.getMessage());
                } else {
                    setData(profileResponse.getData().getProfile());
                }
            }

        });

        binding.name.setText(sharedHelper.getFirstName() + " " + sharedHelper.getLastName());

    }


    private void setData(ProfileData profile) {

        if (profile == null)
            return;

        Log.d("nhcbzxhc",""+profile.getAssignment());
        binding.assignemnts.setText(profile.getAssignment()+" "+getString(R.string.assignment));
        if (profile.getDocumentA() != null) {
            doc1 = profile.getDocumentA();
        }

        if (profile.getDocumentB() != null) {
            doc2 = profile.getDocumentB();
        }

        if (profile.getDriverLicence() != null) {
            licence = profile.getDriverLicence();
        }

        if (profile.getRating() != null && !profile.getRating().equalsIgnoreCase("")) {
            binding.rating.setRating(Float.parseFloat(profile.getRating()));
        }

    }
}
