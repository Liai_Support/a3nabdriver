package com.app.a3d.view.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.app.a3d.databinding.ActivitySplashBinding;
import com.app.a3d.utils.SharedHelper;
import com.google.firebase.iid.FirebaseInstanceId;


public class SplashActivity extends AppCompatActivity {


    Handler handler = new Handler(Looper.getMainLooper());
    Runnable runnable;
    ActivitySplashBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        checkForFcmToken();
//        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);


//        Glide.with(this).load(R.raw.splash_vid).listener(new RequestListener<Drawable>() {
//            @Override
//            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//
//                return false;
//            }
//
//            @Override
//            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                return false;
//            }
//        }).into(binding.splashGif);

//        MediaController mediaController = new MediaController(this);
//        mediaController.setAnchorView(binding.splashGif);
//        binding.splashGif.setMediaController(mediaController);
//        binding.splashGif.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" +
//                R.raw.splash_vid));
//        binding.splashGif.start();
//
//        binding.splashGif.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            @Override
//            public void onCompletion(MediaPlayer mediaPlayer) {
//                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
//
////                if (new SharedHelper(SplashActivity.this).getLoggedIn()) {
////
////                } else {
////                    intent = new Intent(SplashActivity.this, OnBoardActivity.class);
////                }
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//            }
//        });


//        Glide.with(this).asGif().load(R.raw.splash_vid).listener(new RequestListener<GifDrawable>() {
//            @Override
//            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
//        resource.setLoopCount(1);
//        resource.registerAnimationCallback(new Animatable2Compat.AnimationCallback() {
//            @Override
//            public void onAnimationEnd(Drawable drawable) {
//                Intent intent;
//                if (new SharedHelper(SplashActivity.this).getLoggedIn()) {
//                    intent = new Intent(SplashActivity.this, DashboardActivity.class);
//                } else {
//                    intent = new Intent(SplashActivity.this, OnBoardActivity.class);
//                }
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                startActivity(intent);
//
//            }
//        });
//                return false;
//            }
//
//            @Override
//            public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
//
//                return false;
//            }
//
//        }).into(binding.splashGif);

        runnable = () -> {
            Intent intent;
            if (new SharedHelper(SplashActivity.this).getLoggedIn()) {
                intent = new Intent(SplashActivity.this, DashboardActivity.class);
                intent.putExtra("current",0);
            } else {
                intent = new Intent(SplashActivity.this, LoginActivity.class);
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

        };
    }

    private void checkForFcmToken() {

        if (new SharedHelper(this).getFirebaseToken().equalsIgnoreCase("")) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
                String newToken = instanceIdResult.getToken();
                new SharedHelper(this).setFirebaseToken(newToken);
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.postDelayed(runnable, 2000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(runnable);
    }
}
