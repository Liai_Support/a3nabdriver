package com.app.a3d.view.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3d.R;
import com.app.a3d.databinding.ActivityCurrentAssignmentBinding;
import com.app.a3d.interfaces.OnStatusUpdate;
import com.app.a3d.model.CommonResponse;
import com.app.a3d.model.GetRouteResponse;
import com.app.a3d.model.RouteDetails;
import com.app.a3d.model.RoutesPartition;
import com.app.a3d.utils.Constants;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.utils.ImagePicker;
import com.app.a3d.utils.SharedHelper;
import com.app.a3d.utils.Utils;
import com.app.a3d.view.adapter.RoutesAdapter;
import com.app.a3d.viewmodel.AmazonViewModel;
import com.app.a3d.viewmodel.CommonViewModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

public class UpcomingAssignmentActivity extends BaseActivity implements OnMapReadyCallback {


    ActivityCurrentAssignmentBinding binding;
    SharedHelper sharedHelper;
    String routeId = "";
    CommonViewModel commonViewModel;
    AmazonViewModel amazonViewModel;

    GoogleMap map;
    MapFragment mapFragment;

    RoutesAdapter routesAdapter;
    ArrayList<RoutesPartition> routesList = new ArrayList<>();
    String imageUploadpath;


    //imageupload
    String u_id = "";
    String storeI = "", adminNumber = "";

    LatLng nextLatLng = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_current_assignment);
        commonViewModel = new ViewModelProvider(this).get(CommonViewModel.class);
        amazonViewModel = new ViewModelProvider(this).get(AmazonViewModel.class);
        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        sharedHelper = new SharedHelper(this);
        initAdapter();
        setValues();
        getIntentvalues();
        initListener();

    }

    private void initListener() {

        binding.imageView.setOnClickListener(v -> startActivity(new Intent(this, NotificationActivity.class)));

        binding.imageView21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                moveToLocation();
            }
        });

        binding.textView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveToLocation();
            }
        });


    }

    private void moveToLocation() {

        if (nextLatLng != null) {
            String uri = String.format(Locale.ENGLISH, "geo:%f,%f", nextLatLng.latitude, nextLatLng.longitude);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            startActivity(intent);
        }

    }

    private void initAdapter() {

        routesAdapter = new RoutesAdapter(routesList, this, new OnStatusUpdate() {
            @Override
            public void onClick(String position) {

            }
        }, (Uid, storeId) -> {

        }, moveToLastPos -> {


        }, MakeDeliveryPos -> {


        });
        binding.routeDetails.setLayoutManager(new LinearLayoutManager(this));
        binding.routeDetails.setAdapter(routesAdapter);
    }

    private void getIntentvalues() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            routeId = bundle.getString("routeId", "");
            getRoutes();
        }


    }

    private void getRoutes() {

        DialogUtils.showLoader(this);
        commonViewModel.getRoutes(this, routeId).observe(this, getRouteResponse -> {
            DialogUtils.dismissLoader();
            if (getRouteResponse.getError()) {
                Utils.showSnack(binding.parent, getRouteResponse.getMessage());
            } else {

                setDetails(getRouteResponse);


            }
        });

    }

    private void setDetails(GetRouteResponse getRouteResponse) {
        RouteDetails details = getRouteResponse.getData().getRouteDetail();

        binding.orders.setText(details.getOrders());
        binding.pickupLocation.setText(details.getPickupCount());
        binding.dropCount.setText(details.getDropCount());

        binding.assignedDate.setText(getString(R.string.date) + Utils.getConvertedTime(details.getAssignDate(), "dd MMM,yyyy"));
        binding.assignedTime.setText(getString(R.string.time) + Utils.getConvertedTime(details.getAssignDate(), "hh:mm a"));

        if (Utils.getBonusTime(details.getAssignDate()).equalsIgnoreCase(""))
            binding.bonusTime.setText(R.string.bonus_expired);
        else
            binding.bonusTime.setText(new SpannableStringBuilder().append(Utils.getColoredAndBoldString(this, getString(R.string.complete_assignment_in), R.color.edit_text_color))
                    .append(" ")
                    .append(Utils.getColoredAndBoldString(this, Utils.getBonusTime(details.getAssignDate()), R.color.colorPrimary))
                    .append(" ")
                    .append(Utils.getColoredAndBoldString(this, getString(R.string.for_bonus), R.color.edit_text_color)));


        routesList = getRouteResponse.getData().getRoutes();
        routesAdapter.notifyDataChanged(routesList);
        routesAdapter.setAdminNumber(getRouteResponse.getData().getAdminNumber());
        adminNumber = getRouteResponse.getData().getAdminNumber();

        int i = 0;
        while (i < routesList.size()) {
            if (routesList.get(i).getStoreId() != null) {
                if (routesList.get(i).getUsers().size() == 0) {
                    routesList.remove(i);
                } else {
                    i++;
                }
            } else {
                if (routesList.get(i).getStore().size() == 0) {
                    routesList.remove(i);
                } else {
                    i++;
                }
            }
        }

        setMapMarker();

    }

    private void setMapMarker() {

        if (routesList.size() != 0) {
            if (routesList.get(0).getLatitude() != null && routesList.get(0).getLongitude() != null) {
                nextLatLng = new LatLng(Double.parseDouble(routesList.get(0).getLatitude()), Double.parseDouble(routesList.get(0).getLongitude()));
                drawOnMap();
            }
        }
    }

    private void drawOnMap() {

        if (nextLatLng != null && map != null) {
            map.clear();
            map.addMarker(
                    new MarkerOptions().position(nextLatLng).icon(BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            );
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(nextLatLng, 16F));
        }

    }

    private void setValues() {

        binding.textView4.setText(sharedHelper.getFirstName() + " " + sharedHelper.getLastName());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        drawOnMap();
    }


    @Override
    protected void onResume() {
        super.onResume();
        getRoutes();
    }
}
