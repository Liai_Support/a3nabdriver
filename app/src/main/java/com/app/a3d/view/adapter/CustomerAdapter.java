package com.app.a3d.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ChildCustomerBinding;
import com.app.a3d.databinding.ChildDamagesBinding;
import com.app.a3d.interfaces.OnClickListener;
import com.app.a3d.interfaces.OnStatusUpdate;
import com.app.a3d.model.Damages;
import com.app.a3d.model.TopStore;
import com.app.a3d.model.TopUsers;
import com.app.a3d.utils.DialogUtils;

import java.util.ArrayList;

public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.MyViewHolder> {

    Context context;
    ArrayList<TopUsers> list;
    OnStatusUpdate onStatusUpdate;
    OnClickListener onClickListener;


    public CustomerAdapter(Context context,ArrayList<TopUsers> damages, OnStatusUpdate onStatusUpdate,OnClickListener onClickListener) {
        this.context = context;
        this.onStatusUpdate = onStatusUpdate;
        this.onClickListener = onClickListener;
        list = damages;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_customer, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

       // holder.binding.assignemnts.setText(context.getString(R.string.customer_id) + " " + list.get(position).getCustomerID());
        holder.binding.assignemnts.setText(context.getString(R.string.orderid)+String.valueOf(" "+list.get(position).getOrderIds()));

        holder.binding.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(position);
            }
        });

        holder.binding.productList.setLayoutManager(new LinearLayoutManager(context));
        holder.binding.productList.setAdapter(new ProductListAdapter(context, list.get(position).getProduct(), true, new OnClickListener() {
            @Override
            public void onClick(int po) {
                onStatusUpdate.onClick( list.get(position).getProduct().get(po).getId());
            }
        }));
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        else
            return list.size();
    }

    public void setList(ArrayList<TopUsers> damage) {
        list = damage;
        notifyDataSetChanged();
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {
        ChildCustomerBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = ChildCustomerBinding.bind(itemView);

        }
    }
}
