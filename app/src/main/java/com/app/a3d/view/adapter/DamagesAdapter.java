package com.app.a3d.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ChildDamagesBinding;
import com.app.a3d.model.Damages;
import com.app.a3d.view.activity.AssignedCarActivity;

import java.util.ArrayList;

public class DamagesAdapter extends RecyclerView.Adapter<DamagesAdapter.MyViewHolder> {

    Context context;
    ArrayList<Damages> list;

    public DamagesAdapter(Context context, ArrayList<Damages> damages) {
        this.context = context;
        list = damages;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_damages, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.binding.text.setText((position + 1) + ". " + list.get(position).getDamageReason());
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        else
            return list.size();
    }

    public void setList(ArrayList<Damages> damage) {
        list = damage;
        notifyDataSetChanged();
    }

    public void addDamage(String dam) {
        if (list == null) {
            list = new ArrayList<>();
        }
        Damages damages = new Damages();
        damages.setId("0");
        damages.setDamageReason(dam);
        damages.setAddedNow(true);

        list.add(damages);
        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ChildDamagesBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = ChildDamagesBinding.bind(itemView);

        }
    }
}
