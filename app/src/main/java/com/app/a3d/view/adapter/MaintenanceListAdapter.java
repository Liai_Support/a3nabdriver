package com.app.a3d.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ChildDamagesBinding;
import com.app.a3d.databinding.ChildMaintananceListBinding;
import com.app.a3d.interfaces.OnClickListener;
import com.app.a3d.model.Damages;
import com.app.a3d.model.MaintenanceListData;
import com.app.a3d.utils.SharedHelper;

import java.util.ArrayList;

public class MaintenanceListAdapter extends RecyclerView.Adapter<MaintenanceListAdapter.MyViewHolder> {

    Context context;
    ArrayList<MaintenanceListData> list = new ArrayList<>();
    String selectedId = "";
    OnClickListener onClickListener;
    SharedHelper sharedHelper;

    public MaintenanceListAdapter(Context context, ArrayList<MaintenanceListData> list, OnClickListener onClickListener) {
        this.context = context;
        this.list = list;
        this.onClickListener = onClickListener;
        this.sharedHelper = new SharedHelper(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_maintanance_list, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if(sharedHelper.getSelectedLanguage().equalsIgnoreCase("en")){
            holder.binding.gasRefill.setText(list.get(position).getNameList());
        }
        else {
            holder.binding.gasRefill.setText(list.get(position).getNameListar());
        }

        if (list.get(position).getId().equalsIgnoreCase(selectedId)) {
            holder.binding.checkbox.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.maintenance_check));
        } else {
            holder.binding.checkbox.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.maintenance_uncheck));
        }

        holder.binding.gasRefill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(position);
            }
        });
    }

    public void setChecked(String selectedMaintenanceId) {
        selectedId = selectedMaintenanceId;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        else
            return list.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        ChildMaintananceListBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = ChildMaintananceListBinding.bind(itemView);

        }
    }
}
