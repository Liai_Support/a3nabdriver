package com.app.a3d.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ChildCustomerBinding;
import com.app.a3d.databinding.ChildMakeDeliveryBinding;
import com.app.a3d.interfaces.OnClickListener;
import com.app.a3d.interfaces.OnStatusUpdate;
import com.app.a3d.model.RoutesPartition;
import com.app.a3d.model.TopStore;
import com.app.a3d.model.TopUsers;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.utils.GpsUtils;
import com.app.a3d.utils.Utils;
import com.app.a3d.view.activity.MakeDeliveryActivity;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Locale;

public class MakeDeliveryAdapter extends RecyclerView.Adapter<MakeDeliveryAdapter.MyViewHolder> {

    Activity context;
    ArrayList<RoutesPartition> list;
    ArrayList<TopUsers> list1;
    String adminNumber;
    MakeDeliveryActivity makeDeliveryActivity;

    public MakeDeliveryAdapter(MakeDeliveryActivity makeDeliveryActivity,Activity context, ArrayList<RoutesPartition> damages, ArrayList<TopUsers> orderdata, String adminNumber) {
        this.context = context;
        this.makeDeliveryActivity = makeDeliveryActivity;
        list = damages;
        list1 = orderdata;
        this.adminNumber = adminNumber;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_make_delivery, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.binding.textView6.setText(context.getString(R.string.customer) + " " + list.get(position).getCustomerID() + " : " + list.get(position).getFirstName());

        holder.binding.shopListView.setLayoutManager(new LinearLayoutManager(context));
        holder.binding.shopListView.setAdapter(new StoreAdapter(context, list.get(position).getStore(), new OnStatusUpdate() {
            @Override
            public void onClick(String position) {

            }
        }));

        holder.binding.address.setText(new GpsUtils(context).getColoredAndBoldString(context.getString(R.string.address), list.get(position).getAddressPinDetails()));
        holder.binding.orderedOn.setText(new GpsUtils(context).getColoredAndBoldString(context.getString(R.string.ordered_on), Utils.getConvertedTime(list.get(position).getOrderOn(),"hh:mm a '||' dd MMM,yyyy")));
        holder.binding.deliveredBy.setText(new GpsUtils(context).getColoredAndBoldString(context.getString(R.string.deliver_by), Utils.getConvertedTime(list.get(position).getDeliveryDate(),"hh:mm a '||' dd MMM,yyyy")));

        holder.binding.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUtils.callDialog(context, context.getString(R.string.delivery_to_customer_id) + " " + list.get(position).getCustomerID(), list.get(position).getCountryCode() + list.get(position).getMobileNumber(), adminNumber, false);
            }
        });


//        holder.binding.locationNotes.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (list.get(position).getNotes() != null && list.get(position).getNotes().size() != 0)
//                    DialogUtils.viewLocationNotes(context, list.get(position).getNotes().get(0).getCreatedAt(), list.get(position).getNotes().get(0).getDeliveryNotes());
//            }
//        });


        holder.binding.locationNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getLatitude() != null && list.get(position).getLongitude() != null)
                    moveToLocation(new LatLng(Double.parseDouble(list.get(position).getLatitude()), Double.parseDouble(list.get(position).getLongitude())));
            }
        });


        if (list.get(position).getStore() != null) {
            double amount = 0;
            double cbstyleamount = 0;
            String tax = "";
            for (int i = 0; i < list.get(position).getStore().size(); i++) {
                for (int j = 0; j < list.get(position).getStore().get(i).getProduct().size(); j++) {
                   // amount = amount + Double.parseDouble(list.get(position).getStore().get(i).getProduct().get(j).getTotalPrice());
                    amount = amount + ((Integer.parseInt(list.get(position).getStore().get(i).getProduct().get(j).getQuantity())) * (Double.parseDouble(list.get(position).getStore().get(i).getProduct().get(j).getSinglePrice())));
                    if(list.get(position).getStore().get(i).getProduct().get(j).getBoxStylePrice()!=null && list.get(position).getStore().get(i).getProduct().get(j).getCuttingStylePrice()!=null){
                        cbstyleamount = cbstyleamount + ((Integer.parseInt(list.get(position).getStore().get(i).getProduct().get(j).getQuantity())) * (Double.parseDouble(list.get(position).getStore().get(i).getProduct().get(j).getBoxStylePrice()) + Double.parseDouble(list.get(position).getStore().get(i).getProduct().get(j).getCuttingStylePrice())));
                    }
                    else {
                        if(list.get(position).getStore().get(i).getProduct().get(j).getBoxStylePrice()!=null){
                            cbstyleamount = cbstyleamount + ((Integer.parseInt(list.get(position).getStore().get(i).getProduct().get(j).getQuantity())) * (Double.parseDouble(list.get(position).getStore().get(i).getProduct().get(j).getBoxStylePrice())));
                        }
                        else if(list.get(position).getStore().get(i).getProduct().get(j).getCuttingStylePrice()!=null){
                            cbstyleamount = cbstyleamount + ((Integer.parseInt(list.get(position).getStore().get(i).getProduct().get(j).getQuantity())) * (Double.parseDouble(list.get(position).getStore().get(i).getProduct().get(j).getCuttingStylePrice())));
                        }
                    }

                }
            }

            Double subtotal = amount+cbstyleamount;
            Double taxvalue = Double.parseDouble(list.get(position).getTaxValue());
            Double delivery = Double.parseDouble(list.get(position).getFastDelievryCharge());
            //Double subtotal1 = (subtotal-Double.parseDouble(list.get(position).getTakeToCustomer()))+Double.parseDouble(list.get(position).getGiveToCustomer())+delivery;
            if(list.get(position).getOff_types().equalsIgnoreCase("0")){
                delivery = Double.parseDouble(list.get(position).getCouponDiscount().toString());
            }

            Double subtotal1 = subtotal+delivery;
            Double coupendiscount = Double.parseDouble(list.get(position).getCouponDiscount().toString());
            Double walletpoint = Double.parseDouble(list.get(position).getPointsAmount());
            Double walletamount = Double.parseDouble(list.get(position).getPaidByWallet());
            Double vat = ((subtotal1-(coupendiscount+walletpoint+walletamount)) * (taxvalue / 100));
            Double total = ((subtotal1-(coupendiscount+walletpoint+walletamount))+vat);
            if(total < 0){
                total = 0.0;
            }
            if(vat <= 0){
                vat = 0.0;
            }
            makeDeliveryActivity.amount = total;

            holder.binding.taxTxt.setText(""+context.getString(R.string.tax_1)+"("+taxvalue+" %)");
            holder.binding.productTotal.setText(""+String.format(Locale.ENGLISH,"%.2f",subtotal)+ " SAR");
            holder.binding.deliveryVal.setText(""+String.format(Locale.ENGLISH,"%.2f",delivery)+" SAR");
            holder.binding.tax1.setText(""+String.format(Locale.ENGLISH,"%.2f",vat)+" SAR");
            holder.binding.discount.setText("-"+String.format(Locale.ENGLISH,"%.2f",coupendiscount)+" SAR");
            holder.binding.waletpointVal.setText(""+String.format(Locale.ENGLISH,"%.2f",walletpoint)+" SAR");
            holder.binding.walletamountVal.setText(""+String.format(Locale.ENGLISH,"%.2f",walletamount)+" SAR");
            holder.binding.totalBill.setText(""+String.format(Locale.ENGLISH,"%.2f",total)+ " SAR");

            holder.binding.text12Value.setText(""+list.get(position).getTakeToCustomer()+ " SAR");
            holder.binding.text13Value.setText(""+list.get(position).getGiveToCustomer()+ " SAR");

            if(walletpoint==0.0){
                holder.binding.waletpointVal.setVisibility(View.GONE);
                holder.binding.walletpointTxt.setVisibility(View.GONE);
            }
            if(coupendiscount==0.0){
                holder.binding.discountTxt.setVisibility(View.GONE);
                holder.binding.discount.setVisibility(View.GONE);
            }
            if(walletamount==0.0){
                holder.binding.walletamountTxt.setVisibility(View.GONE);
                holder.binding.walletamountVal.setVisibility(View.GONE);
            }

        }

        Log.d("hcj",""+list.get(position).getPayType());
        if(list.get(position).getPayType().equalsIgnoreCase("CASH")){
            holder.binding.modeOfPayment.setText(""+context.getString(R.string.codmachine));
        }
        else {
            holder.binding.modeOfPayment.setText(list.get(position).getPayType());
        }

    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        else
            return list.size();
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {
        ChildMakeDeliveryBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = ChildMakeDeliveryBinding.bind(itemView);

        }
    }


    private void moveToLocation(LatLng nextLatLng) {
        Log.d("dcnmxc0",""+nextLatLng);
        if (nextLatLng != null) {
            String uri = String.format(Locale.ENGLISH, "google.navigation:q="+nextLatLng.latitude+","+nextLatLng.longitude);
            // String uri = String.format(Locale.ENGLISH, "geo:%f,%f", nextLatLng.latitude, nextLatLng.longitude);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            intent.setPackage("com.google.android.apps.maps");
            context.startActivity(intent);
         /*   String uri = String.format(Locale.ENGLISH, "geo:%f,%f", nextLatLng.latitude, nextLatLng.longitude);
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            context.startActivity(intent);*/
        }

    }
}
