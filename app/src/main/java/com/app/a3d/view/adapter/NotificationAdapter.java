package com.app.a3d.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ChildCustomerBinding;
import com.app.a3d.databinding.ChildNotificationBinding;
import com.app.a3d.interfaces.OnClickListener;
import com.app.a3d.interfaces.OnStatusUpdate;
import com.app.a3d.model.NotificationList;
import com.app.a3d.model.TopUsers;
import com.app.a3d.utils.Utils;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    Context context;
    ArrayList<NotificationList> list;


    public NotificationAdapter(Context context, ArrayList<NotificationList> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_notification, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        if (list.get(position).getNotifyType().equalsIgnoreCase("ASSIGN_ORDER")) {

            holder.binding.imageView35.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.order_notification_image));

            holder.binding.title.setText(context.getString(R.string.recived_new_assignment));

        } else if (list.get(position).getNotifyType().equalsIgnoreCase("FLOATING_CASH")) {

            holder.binding.imageView35.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.dollar_notification_image));
            holder.binding.title.setText(context.getString(R.string.floating_cash_submit) +" SAR "+list.get(position).getAmount());

        }


        holder.binding.date.setText(Utils.getConvertedTime(list.get(position).getCreated_at(),"dd MMMM,yyyy"));


    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        else
            return list.size();
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {
        ChildNotificationBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = ChildNotificationBinding.bind(itemView);

        }
    }
}
