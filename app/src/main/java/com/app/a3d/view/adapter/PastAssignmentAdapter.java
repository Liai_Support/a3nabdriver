package com.app.a3d.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ChildAssignmentsBinding;
import com.app.a3d.databinding.ChildDamagesBinding;
import com.app.a3d.model.Assignment;
import com.app.a3d.model.Damages;
import com.app.a3d.view.activity.PastAssignmentActivity;

import java.util.ArrayList;

public class PastAssignmentAdapter extends RecyclerView.Adapter<PastAssignmentAdapter.MyViewHolder> {

    Context context;
    ArrayList<Assignment> list;

    public PastAssignmentAdapter(Context context, ArrayList<Assignment> damages) {
        this.context = context;
        list = damages;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_assignments, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.binding.assignmentId.setText(context.getString(R.string.assignment) + " #" + list.get(position).getId());

        holder.binding.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, PastAssignmentActivity.class).putExtra("routeId", list.get(position).getId()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {
        ChildAssignmentsBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = ChildAssignmentsBinding.bind(itemView);

        }
    }
}
