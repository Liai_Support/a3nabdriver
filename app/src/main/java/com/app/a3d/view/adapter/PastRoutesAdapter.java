package com.app.a3d.view.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ChildGotoDeliveryLocationBinding;
import com.app.a3d.databinding.ChildGotoMeetShopBinding;
import com.app.a3d.databinding.ChildPastGotoDeliveryBinding;
import com.app.a3d.databinding.ChildPastGotoMeetShopBinding;
import com.app.a3d.databinding.ChildPastGotoMeetShopBindingImpl;
import com.app.a3d.interfaces.OnClickListener;
import com.app.a3d.interfaces.OnStatusUpdate;
import com.app.a3d.interfaces.onReciptuploadClicked;
import com.app.a3d.model.PastRoutesPartition;
import com.app.a3d.model.RoutesPartition;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.utils.Utils;

import java.util.ArrayList;

public class PastRoutesAdapter extends RecyclerView.Adapter<PastRoutesAdapter.RoutesViewHolder> {

    ArrayList<PastRoutesPartition> routeList = new ArrayList<>();
    Activity context;
    String adminNumber;


    public PastRoutesAdapter(ArrayList<PastRoutesPartition> routeList, Activity context) {
        this.routeList = routeList;
        this.context = context;


    }

    @NonNull
    @Override
    public RoutesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            return new RoutesViewHolder(LayoutInflater.from(context).inflate(R.layout.child_past_goto_meet_shop, parent, false), 0);
        } else {
            return new RoutesViewHolder(LayoutInflater.from(context).inflate(R.layout.child_past_goto_delivery, parent, false), 1);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RoutesViewHolder holder, int position) {

        if (routeList.get(position).getStoreId() != null) {
            //logic for store
            if (holder.shopBinding != null)
                storeBindingAdapter(position, holder);
        } else {
            //login for deliveryLocation
            if (holder.deliveryBinding != null)
                deliveryBindingAdapter(position, holder);
        }
    }

    private void deliveryBindingAdapter(int position, RoutesViewHolder holder) {

        holder.deliveryBinding.imageView25.setText((position + 1) + "");
        holder.deliveryBinding.assignemnts.setText(context.getString(R.string.delivery_to_customer_id) + " " +
                routeList.get(position).getCustomerID());


        holder.deliveryBinding.recyclerView.setLayoutManager(new LinearLayoutManager(context));

        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < routeList.get(position).getStore().size(); i++) {
            for (int j = 0; j < routeList.get(position).getStore().get(i).getProduct().size(); j++)
                list.add(context.getString(R.string.purchased) + " " + routeList.get(position).getStore().get(i).getProduct().get(j).getQuantity() + " " + routeList.get(position).getStore().get(i).getProduct().get(j).getProductName());
        }

        holder.deliveryBinding.recyclerView.setAdapter(new PastStatusAdapter(context, list));


        holder.deliveryBinding.openStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                routeList.get(position).setExpanded(!routeList.get(position).isExpanded());
                notifyItemChanged(position);
            }
        });

        if (routeList.get(position).isExpanded()) {
            holder.deliveryBinding.recyclerView.setVisibility(View.VISIBLE);
            holder.deliveryBinding.openStatus.setImageTintList(ContextCompat.getColorStateList(context, R.color.orange));
            holder.deliveryBinding.openStatus.setRotation(0);
        } else {
            holder.deliveryBinding.recyclerView.setVisibility(View.GONE);
            holder.deliveryBinding.openStatus.setImageTintList(ContextCompat.getColorStateList(context, R.color.edit_text_color));
            holder.deliveryBinding.openStatus.setRotation(180);
        }


    }

    private void storeBindingAdapter(int position, RoutesViewHolder holder) {

        holder.shopBinding.imageView25.setText((position + 1) + "");
        holder.shopBinding.assignemnts.setText(context.getString(R.string.go_to) + " " +
                routeList.get(position).getStoreName()
                + " " + context.getString(R.string.shop));

        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < routeList.get(position).getUsers().size(); i++) {
            for (int j = 0; j < routeList.get(position).getUsers().get(i).getProduct().size(); j++)
                list.add(context.getString(R.string.purchased) + " " + routeList.get(position).getUsers().get(i).getProduct().get(j).getQuantity() + " " + routeList.get(position).getUsers().get(i).getProduct().get(j).getProductName());
        }
        holder.shopBinding.customerList.setLayoutManager(new LinearLayoutManager(context));
        holder.shopBinding.customerList.setAdapter(new PastStatusAdapter(context, list));


        if (routeList.get(position).isExpanded()) {
            holder.shopBinding.customerList.setVisibility(View.VISIBLE);
            holder.shopBinding.openStatus.setImageTintList(ContextCompat.getColorStateList(context, R.color.colorPrimary));
            holder.shopBinding.openStatus.setRotation(0);

        } else {
            holder.shopBinding.openStatus.setImageTintList(ContextCompat.getColorStateList(context, R.color.edit_text_color));
            holder.shopBinding.customerList.setVisibility(View.GONE);
            holder.shopBinding.openStatus.setRotation(180);
        }

        holder.shopBinding.openStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                routeList.get(position).setExpanded(!routeList.get(position).isExpanded());
                notifyItemChanged(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return routeList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (routeList.get(position).getStoreId() != null) {
            return 0;
        } else {
            return 1;
        }
    }


    class RoutesViewHolder extends RecyclerView.ViewHolder {

        ChildPastGotoMeetShopBinding shopBinding;
        ChildPastGotoDeliveryBinding deliveryBinding;

        public RoutesViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            if (viewType == 0) {
                shopBinding = ChildPastGotoMeetShopBinding.bind(itemView);
            } else {
                deliveryBinding = ChildPastGotoDeliveryBinding.bind(itemView);
            }
        }
    }
}
