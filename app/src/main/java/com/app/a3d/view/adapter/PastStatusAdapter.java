package com.app.a3d.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ChildPastRouteDetailsWithStatusBinding;
import com.app.a3d.databinding.ChildUpcomingAssignmentsBinding;
import com.app.a3d.model.Assignment;
import com.app.a3d.utils.Utils;

import java.util.ArrayList;

public class PastStatusAdapter extends RecyclerView.Adapter<PastStatusAdapter.MyViewHolder> {

    Context context;
    ArrayList<String> list;

    public PastStatusAdapter(Context context, ArrayList<String> damages) {
        this.context = context;
        list = damages;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_past_route_details_with_status, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.binding.productName.setText(list.get(position));

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {
        ChildPastRouteDetailsWithStatusBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = ChildPastRouteDetailsWithStatusBinding.bind(itemView);

        }
    }
}
