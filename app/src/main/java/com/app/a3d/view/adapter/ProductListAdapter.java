package com.app.a3d.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ChildCustomerBinding;
import com.app.a3d.databinding.ChildProductCustomerBinding;
import com.app.a3d.interfaces.OnClickListener;
import com.app.a3d.model.ProductsList;
import com.app.a3d.model.TopUsers;
import com.app.a3d.utils.SharedHelper;
import com.app.a3d.utils.Utils;

import java.util.ArrayList;
import java.util.Locale;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder> {

    Context context;
    ArrayList<ProductsList> list;
    Boolean isStore;
    OnClickListener onClickListener;

    public ProductListAdapter(Context context, ArrayList<ProductsList> damages, Boolean isStore, OnClickListener onClickListener) {
        this.context = context;
        this.isStore = isStore;
        this.onClickListener = onClickListener;
        list = damages;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_product_customer, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Log.d("vxc",""+list.get(position).getProductImage());
        Log.d("xcvxc","dvxdv");
        Utils.loadImage(holder.binding.productImage, list.get(position).getProductImage());

        if(new SharedHelper(context).getSelectedLanguage().equalsIgnoreCase("ar")){
            holder.binding.productName.setText(list.get(position).getArabicName());
        }
        else {
            holder.binding.productName.setText(list.get(position).getProductName());
        }
        holder.binding.quandity.setText("X " + list.get(position).getQuantity());
        double valu1 = 0;
        double singlecbvalue = 0;
        if(list.get(position).getBoxStylePrice()!=null && list.get(position).getCuttingStylePrice()!=null){
            valu1 = (Double.parseDouble(list.get(position).getQuantity()))*(Double.parseDouble(list.get(position).getBoxStylePrice()) + Double.parseDouble(list.get(position).getCuttingStylePrice()));
            singlecbvalue = (Double.parseDouble(list.get(position).getBoxStylePrice()) + Double.parseDouble(list.get(position).getCuttingStylePrice()));
        }
        else {
            if(list.get(position).getBoxStylePrice()!=null){
                valu1 = (Double.parseDouble(list.get(position).getQuantity()))*(Double.parseDouble(list.get(position).getBoxStylePrice()));
                singlecbvalue = (Double.parseDouble(list.get(position).getBoxStylePrice()) + Double.parseDouble(list.get(position).getCuttingStylePrice()));
            }
            else if(list.get(position).getCuttingStylePrice()!=null){
                valu1 = (Double.parseDouble(list.get(position).getQuantity()))*(Double.parseDouble(list.get(position).getCuttingStylePrice()));
                singlecbvalue = (Double.parseDouble(list.get(position).getBoxStylePrice()) + Double.parseDouble(list.get(position).getCuttingStylePrice()));
            }
        }

        holder.binding.amount.setText(""+String.format(Locale.ENGLISH,"%.2f",((Double.parseDouble(list.get(position).getSinglePrice())+singlecbvalue))));
        double tot = (Double.parseDouble(list.get(position).getSinglePrice())*Integer.parseInt(list.get(position).getQuantity())) + (valu1);
        holder.binding.totalAmount.setText(String.format(Locale.ENGLISH,"%.2f",tot));
        if(!list.get(position).getSpecialInstructions().equals("")){
            holder.binding.specialInstruction.setText(list.get(position).getSpecialInstructions());
        }

        if (list.get(position).isPickUp() == 1) {
            holder.binding.status.setImageTintList(ContextCompat.getColorStateList(context, R.color.colorPrimary));
        } else {
            holder.binding.status.setImageTintList(ContextCompat.getColorStateList(context, R.color.edit_text_color));
        }

        holder.binding.status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).isPickUp() == 0 && isStore) {
                    onClickListener.onClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        else
            return list.size();
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {
        ChildProductCustomerBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = ChildProductCustomerBinding.bind(itemView);

        }
    }
}

