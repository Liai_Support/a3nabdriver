package com.app.a3d.view.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ChildGotoDeliveryLocationBinding;
import com.app.a3d.databinding.ChildGotoMeetShopBinding;
import com.app.a3d.interfaces.OnClickListener;
import com.app.a3d.interfaces.OnStatusUpdate;
import com.app.a3d.interfaces.onReciptuploadClicked;
import com.app.a3d.model.GetRouteResponse;
import com.app.a3d.model.RoutesPartition;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.utils.Utils;

import java.util.ArrayList;

public class RoutesAdapter extends RecyclerView.Adapter<RoutesAdapter.RoutesViewHolder> {

    ArrayList<RoutesPartition> routeList = new ArrayList<>();
    Activity context;
    String adminNumber;
    OnStatusUpdate onStatusUpdate;
    onReciptuploadClicked onReciptUpdate;
    OnClickListener moveToLast;
    OnClickListener makeDelivery;

    public RoutesAdapter(ArrayList<RoutesPartition> routeList, Activity context, OnStatusUpdate onStatusUpdate, onReciptuploadClicked onReciptUpdate, OnClickListener moveToLast, OnClickListener makeDelivery) {
        this.routeList = routeList;
        this.context = context;
        this.onStatusUpdate = onStatusUpdate;
        this.onReciptUpdate = onReciptUpdate;
        this.moveToLast = moveToLast;
        this.makeDelivery = makeDelivery;
    }

    @NonNull
    @Override
    public RoutesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            return new RoutesViewHolder(LayoutInflater.from(context).inflate(R.layout.child_goto_meet_shop, parent, false), 0);
        } else {
            return new RoutesViewHolder(LayoutInflater.from(context).inflate(R.layout.child_goto_delivery_location, parent, false), 1);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RoutesViewHolder holder, int position) {

        if (routeList.get(position).getStoreId() != null) {
            //logic for store
            if (holder.shopBinding != null)
                storeBindingAdapter(position, holder);
        } else {
            //login for deliveryLocation
            if (holder.deliveryBinding != null)
                deliveryBindingAdapter(position, holder);
        }
    }

    private void deliveryBindingAdapter(int position, RoutesViewHolder holder) {

        holder.deliveryBinding.imageView25.setText((position + 1) + "");
      //  holder.deliveryBinding.assignemnts.setText(context.getString(R.string.delivery_to_customer_id) + " " + routeList.get(position).getCustomerID());
        holder.deliveryBinding.assignemnts.setText(context.getString(R.string.delivery_to_customer) +" " + routeList.get(position).getFirstName());


        holder.deliveryBinding.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("jhbjbjh",routeList.get(position).getCountryCode()+"gvg"+""+routeList.get(position).getMobileNumber());
                DialogUtils.callDialog(context, context.getString(R.string.delivery_to_customer) + " " + routeList.get(position).getFirstName(), routeList.get(position).getCountryCode() + routeList.get(position).getMobileNumber(), adminNumber, false);

            }
        });

        holder.deliveryBinding.recyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.deliveryBinding.recyclerView.setAdapter(new StoreAdapter(context, routeList.get(position).getStore(), new OnStatusUpdate() {
            @Override
            public void onClick(String position) {
//                onStatusUpdate.onClick(position);
            }
        }));

        holder.deliveryBinding.locationNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (routeList.get(position).getNotes() != null && routeList.get(position).getNotes().size() != 0) {
                    DialogUtils.viewLocationNotes(context, Utils.getConvertedTime(routeList.get(position).getNotes().get(0).getCreatedAt(), "dd MMM,yyyy"), routeList.get(position).getNotes().get(0).getDeliveryNotes());
                } else {
                    Utils.showSnack(context.findViewById(android.R.id.content), context.getString(R.string.no_loc_notes));
                }
            }
        });

        holder.deliveryBinding.openStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                routeList.get(position).setExpanded(!routeList.get(position).isExpanded());
                notifyItemChanged(position);
            }
        });
        holder.deliveryBinding.assignemnts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                routeList.get(position).setExpanded(!routeList.get(position).isExpanded());
                notifyItemChanged(position);
            }
        });

        if (!routeList.get(position).isExpanded()) {
            holder.deliveryBinding.group.setVisibility(View.VISIBLE);
            holder.deliveryBinding.dividerView.setVisibility(View.GONE);
            holder.deliveryBinding.openStatus.setImageTintList(ContextCompat.getColorStateList(context, R.color.orange));
            holder.deliveryBinding.openStatus.setRotation(0);
        } else {
            holder.deliveryBinding.openStatus.setImageTintList(ContextCompat.getColorStateList(context, R.color.edit_text_color));
            holder.deliveryBinding.group.setVisibility(View.GONE);
            holder.deliveryBinding.dividerView.setVisibility(View.GONE);
            holder.deliveryBinding.openStatus.setRotation(180);
        }


        holder.deliveryBinding.movetoLast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (routeList.size() != 1) {
                    moveToLast.onClick(position);
                }
            }
        });


        holder.deliveryBinding.makeDelivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < routeList.get(position).getStore().size(); i++) {
                    for (int j = 0; j < routeList.get(position).getStore().get(i).getProduct().size(); j++) {
                        if (routeList.get(position).getStore().get(i).getProduct().get(j).isPickUp() == 0) {
                            return;
                        }
                    }
                }
                makeDelivery.onClick(position);

            }
        });

    }

    private void storeBindingAdapter(int position, RoutesViewHolder holder) {

        holder.shopBinding.imageView25.setText((position + 1) + "");
        holder.shopBinding.assignemnts.setText(context.getString(R.string.go_to) + " " +
                routeList.get(position).getStoreName()
                + " " + context.getString(R.string.shop));


        holder.shopBinding.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("jhbjefesfbjh",routeList.get(position).getCountryCode()+"gvg"+""+routeList.get(position).getMobileNumber());
                if(routeList.get(position).getCountryCode() == null){
                    DialogUtils.callDialog(context, routeList.get(position).getStoreName(), "966"+routeList.get(position).getMobileNumber(), adminNumber, true);
                }else {
                    DialogUtils.callDialog(context, routeList.get(position).getStoreName(), routeList.get(position).getCountryCode() + routeList.get(position).getMobileNumber(), adminNumber, true);
                }

            }
        });

        holder.shopBinding.customerList.setLayoutManager(new LinearLayoutManager(context));
        holder.shopBinding.customerList.setAdapter(new CustomerAdapter(context,routeList.get(position).getUsers(), position1 -> {
            onStatusUpdate.onClick(position1);
        }, position2 -> {
            onReciptUpdate.onClick(routeList.get(position).getUsers().get(position2).getU_id(), routeList.get(position).getStoreId());
        }));

        Log.d("dnvgds",""+routeList.get(position).isExpanded());
        if (routeList.get(position).isExpanded()) {
            holder.shopBinding.divider.setVisibility(View.GONE);
            holder.shopBinding.customerList.setVisibility(View.VISIBLE);
            holder.shopBinding.openStatus.setImageTintList(ContextCompat.getColorStateList(context, R.color.colorPrimary));
            holder.shopBinding.openStatus.setRotation(0);

        } else {
            holder.shopBinding.openStatus.setImageTintList(ContextCompat.getColorStateList(context, R.color.edit_text_color));
            holder.shopBinding.divider.setVisibility(View.GONE);
            holder.shopBinding.customerList.setVisibility(View.GONE);
            holder.shopBinding.openStatus.setRotation(180);
        }

        holder.shopBinding.openStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                routeList.get(position).setExpanded(!routeList.get(position).isExpanded());
                notifyItemChanged(position);
            }
        });
        holder.shopBinding.assignemnts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                routeList.get(position).setExpanded(!routeList.get(position).isExpanded());
                notifyItemChanged(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return routeList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (routeList.get(position).getStoreId() != null) {
            return 0;
        } else {
            return 1;
        }
    }

    public void notifyDataChanged(ArrayList<RoutesPartition> routesList) {
        routeList = routesList;
        notifyDataSetChanged();
    }

    public void setAdminNumber(String number) {
        adminNumber = number;
    }

    class RoutesViewHolder extends RecyclerView.ViewHolder {

        ChildGotoMeetShopBinding shopBinding;
        ChildGotoDeliveryLocationBinding deliveryBinding;

        public RoutesViewHolder(@NonNull View itemView, int viewType) {
            super(itemView);
            if (viewType == 0) {
                shopBinding = ChildGotoMeetShopBinding.bind(itemView);
            } else {
                deliveryBinding = ChildGotoDeliveryLocationBinding.bind(itemView);
            }
        }
    }
}
