package com.app.a3d.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ChildCustomerBinding;
import com.app.a3d.interfaces.OnClickListener;
import com.app.a3d.interfaces.OnStatusUpdate;
import com.app.a3d.model.TopStore;
import com.app.a3d.model.TopUsers;

import java.util.ArrayList;

public class StoreAdapter extends RecyclerView.Adapter<StoreAdapter.MyViewHolder> {

    Context context;
    ArrayList<TopStore> list;
    OnStatusUpdate onStatusUpdate;

    public StoreAdapter(Context context, ArrayList<TopStore> damages, OnStatusUpdate onStatusUpdate) {
        this.context = context;
        this.onStatusUpdate = onStatusUpdate;
        list = damages;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_customer, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") int position) {

        if(list.get(position).getProduct().size() != 0){
            holder.binding.call.setVisibility(View.INVISIBLE);
            holder.binding.callTxt.setVisibility(View.INVISIBLE);
            holder.binding.imageView27.setVisibility(View.INVISIBLE);
            holder.binding.dum.setVisibility(View.INVISIBLE);
            holder.binding.assignemnts.setText(list.get(position).getStoreName());
            holder.binding.call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //take picture
                }
            });

            holder.binding.productList.setLayoutManager(new LinearLayoutManager(context));
            holder.binding.productList.setAdapter(new ProductListAdapter(context, list.get(position).getProduct(), false, new OnClickListener() {
                @Override
                public void onClick(int po) {
                    onStatusUpdate.onClick(list.get(position).getProduct().get(po).getId());
                }
            }));
        }
        else {
           // holder.binding.getRoot().setVisibility(View.GONE);
            holder.binding.call.setVisibility(View.GONE);
            holder.binding.callTxt.setVisibility(View.GONE);
            holder.binding.imageView27.setVisibility(View.GONE);
            holder.binding.dum.setVisibility(View.GONE);
            holder.binding.productList.setVisibility(View.GONE);
            holder.binding.assignemnts.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (list == null)
            return 0;
        else
            return list.size();
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {
        ChildCustomerBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = ChildCustomerBinding.bind(itemView);

        }
    }
}
