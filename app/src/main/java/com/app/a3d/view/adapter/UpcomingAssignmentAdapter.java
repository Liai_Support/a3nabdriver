package com.app.a3d.view.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.a3d.R;
import com.app.a3d.databinding.ChildAssignmentsBinding;
import com.app.a3d.databinding.ChildUpcomingAssignmentsBinding;
import com.app.a3d.model.Assignment;
import com.app.a3d.utils.Utils;
import com.app.a3d.view.activity.CurrentAssignmentActivity;
import com.app.a3d.view.activity.DashboardActivity;
import com.app.a3d.view.activity.UpcomingAssignmentActivity;

import java.util.ArrayList;

public class UpcomingAssignmentAdapter extends RecyclerView.Adapter<UpcomingAssignmentAdapter.MyViewHolder> {

    Context context;
    ArrayList<Assignment> list;

    public UpcomingAssignmentAdapter(Context context, ArrayList<Assignment> damages) {
        this.context = context;
        list = damages;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.child_upcoming_assignments, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.binding.assignmentId.setText(context.getString(R.string.assignment) + ". " + list.get(position).getId());

        holder.binding.assignedDate.setText(context.getString(R.string.date) + Utils.getConvertedTime(list.get(position).getAssignDate(), "dd MMMM,YYYY"));
        holder.binding.assignedTime.setText(context.getString(R.string.time) + Utils.getConvertedTime(list.get(position).getAssignDate(), "hh:mm a"));


        holder.binding.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, UpcomingAssignmentActivity.class)
                        .putExtra("routeId", list.get(position).getId()));
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {
        ChildUpcomingAssignmentsBinding binding;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            binding = ChildUpcomingAssignmentsBinding.bind(itemView);

        }
    }
}
