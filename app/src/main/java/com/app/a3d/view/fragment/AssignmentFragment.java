package com.app.a3d.view.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.a3d.R;
import com.app.a3d.databinding.ActivityProfileBinding;
import com.app.a3d.databinding.FragmentAssignmentBinding;
import com.app.a3d.databinding.FragmentHomeBinding;
import com.app.a3d.model.AssignmentData;
import com.app.a3d.model.AssignmentResponse;
import com.app.a3d.model.CommonResponse;
import com.app.a3d.model.ProfileData;
import com.app.a3d.model.ProfileResponse;
import com.app.a3d.utils.Constants;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.utils.ImagePicker;
import com.app.a3d.utils.SharedHelper;
import com.app.a3d.utils.Utils;
import com.app.a3d.view.activity.CurrentAssignmentActivity;
import com.app.a3d.view.activity.DashboardActivity;
import com.app.a3d.view.activity.EditProfileActivity;
import com.app.a3d.view.activity.LoginActivity;
import com.app.a3d.view.activity.NotificationActivity;
import com.app.a3d.view.adapter.PastAssignmentAdapter;
import com.app.a3d.view.adapter.UpcomingAssignmentAdapter;
import com.app.a3d.viewmodel.AmazonViewModel;
import com.app.a3d.viewmodel.CommonViewModel;

import java.io.File;

public class AssignmentFragment extends Fragment {


    FragmentAssignmentBinding binding;
    SharedHelper sharedHelper;
    CommonViewModel commonViewModel;

    String currentAssignmentId = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View layout = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_assignment, container, false);
        binding = FragmentAssignmentBinding.bind(layout);
        sharedHelper = new SharedHelper(requireContext());
        commonViewModel = new ViewModelProvider(this).get(CommonViewModel.class);

        initListener();
        return binding.getRoot();
    }

    private void initListener() {
        binding.cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!currentAssignmentId.equalsIgnoreCase("")) {
                    startActivity(new Intent(requireContext(), CurrentAssignmentActivity.class)
                            .putExtra("routeId", currentAssignmentId));
                }

            }
        });

        binding.imageView.setOnClickListener(v -> startActivity(new Intent(requireContext(), NotificationActivity.class)));
    }

    @Override
    public void onResume() {
        super.onResume();
        getAssignmentList();
    }

    private void getAssignmentList() {

        commonViewModel.getAssignmetnList(requireContext()).observe(getViewLifecycleOwner(), new Observer<AssignmentResponse>() {
            @Override
            public void onChanged(AssignmentResponse assignmentResponse) {
                if (assignmentResponse.getError()) {
                    Utils.showSnack(binding.parent, assignmentResponse.getMessage());
                } else {
                    if (assignmentResponse.getData().getCurrent().size() != 0) {
                        binding.cardView2.setVisibility(View.VISIBLE);
                        currentAssignmentId = assignmentResponse.getData().getCurrent().get(0).getId();
                        binding.assignment.setText(getString(R.string.assignment) + " #" + assignmentResponse.getData().getCurrent().get(0).getId());
                    } else {
                        binding.cardView2.setVisibility(View.GONE);
                    }

                    setAdapter(assignmentResponse.getData());
                }
            }
        });
    }

    private void setAdapter(AssignmentData data) {

        if (data.getCompleted().size() == 0) {
            binding.groupPast.setVisibility(View.GONE);
        } else {
            binding.groupPast.setVisibility(View.VISIBLE);

            binding.pastAssignment.setLayoutManager(new LinearLayoutManager(requireContext()));
            binding.pastAssignment.setAdapter(new PastAssignmentAdapter(requireContext(), data.getCompleted()));
        }

        if (data.getUpcoming().size() == 0) {
            binding.groupUpcoming.setVisibility(View.GONE);
        } else {
            binding.groupUpcoming.setVisibility(View.VISIBLE);

            binding.recyclerView3.setLayoutManager(new LinearLayoutManager(requireContext()));
            binding.recyclerView3.setAdapter(new UpcomingAssignmentAdapter(requireContext(), data.getUpcoming()));
        }

    }


}
