package com.app.a3d.view.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.app.a3d.R;
import com.app.a3d.databinding.FragmentHomeBinding;
import com.app.a3d.model.CarDetails;
import com.app.a3d.model.CommonResponse;
import com.app.a3d.model.CurrentAssignment;
import com.app.a3d.model.DashboardData;
import com.app.a3d.model.DashboardResponse;
import com.app.a3d.model.NewCar;
import com.app.a3d.model.ProfileData;
import com.app.a3d.model.ProfileResponse;
import com.app.a3d.utils.Constants;
import com.app.a3d.utils.DialogUtils;
import com.app.a3d.utils.GpsUtils;
import com.app.a3d.utils.SharedHelper;
import com.app.a3d.utils.Utils;
import com.app.a3d.view.activity.AssignedCarActivity;
import com.app.a3d.view.activity.CurrentAssignmentActivity;
import com.app.a3d.view.activity.DashboardActivity;
import com.app.a3d.view.activity.NotificationActivity;
import com.app.a3d.view.activity.PendingCarActivity;
import com.app.a3d.view.activity.ProfileActivity;
import com.app.a3d.viewmodel.CommonViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    FragmentHomeBinding binding;
    SharedHelper sharedHelper;
    CommonViewModel commonViewModel;

    private String currentAssignmentRouteId = "";

    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private FusedLocationProviderClient fusedLocationClient;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View layout = LayoutInflater.from(requireContext()).inflate(R.layout.fragment_home, container, false);
        binding = FragmentHomeBinding.bind(layout);
        sharedHelper = new SharedHelper(requireContext());
        commonViewModel = new ViewModelProvider(this).get(CommonViewModel.class);
        setValues();
        initListener();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext());

        return binding.getRoot();

    }


    private void locationListener() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval((20 * 1000));

        locationCallback = new LocationCallback() {

            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {
                        getLastKnownLocation();
                    }
                }

            }

        };


    }

    private void askLocationPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (requireContext().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && requireContext().checkSelfPermission(
                    Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                        Constants.Permissions.LOCATIONPERMISSION_LIST,
                        Constants.Permissions.LOCATIONREQUEST_CODE
                );

            } else {
                getUserLocation();
            }
        } else {
            getUserLocation();
        }
    }

    public void onFragmentResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.RequestCode.GPS_REQUEST) {
            if (resultCode == Activity.RESULT_OK)
                getLastKnownLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constants.Permissions.LOCATIONREQUEST_CODE) {
            if (grantResults.length >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                getUserLocation();
            }
        }

    }

    private void getUserLocation() {
        new GpsUtils(requireActivity()).turnGPSOn(isGPSEnable -> {
            if (isGPSEnable) {
                getLastKnownLocation();
            }
        });
    }


    private void getLastKnownLocation() {


        if ((!isDetached()) && isVisible()) {
            if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            fusedLocationClient.getLastLocation().addOnSuccessListener(requireActivity(), new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {

                    if (location != null) {
                        String currentLat = String.valueOf(location.getLatitude());
                        String currentLng = String.valueOf(location.getLongitude());

                        commonViewModel.updateLatLng(requireContext(), currentLat, currentLng);

                    } else {
                        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        fusedLocationClient.requestLocationUpdates(
                                locationRequest,
                                locationCallback,
                                Looper.getMainLooper()
                        );
                    }

                }
            });
        }


    }

    private void initListener() {

        binding.cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(requireContext(), CurrentAssignmentActivity.class)
                        .putExtra("routeId", currentAssignmentRouteId));
            }
        });
        binding.imageView.setOnClickListener(v -> startActivity(new Intent(requireContext(), NotificationActivity.class)));
        binding.layoutOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOnline(1);
            }
        });
        binding.layoutOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOnline(0);
            }
        });
        binding.name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(requireContext(), ProfileActivity.class));
            }
        });

        binding.assignedCardView.setOnClickListener(v -> {

            Intent intent = new Intent(requireContext(), AssignedCarActivity.class);
            startActivity(intent);

        });

        binding.view3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(requireContext(), PendingCarActivity.class));
            }
        });
    }

    private void setOnline(int i) {

        DialogUtils.showLoader(requireContext());
        commonViewModel.updateStatus(requireContext(), i).observe(this, new Observer<CommonResponse>() {
            @Override
            public void onChanged(CommonResponse commonResponse) {
                DialogUtils.dismissLoader();
                if (commonResponse.getError()) {
                    Utils.showSnack(binding.parentView, commonResponse.getMessage());
                } else {
                    setOnlineStatus(String.valueOf(i));
                }
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        getDashBoardDetails();
        binding.name.setText(sharedHelper.getFirstName() + " " + sharedHelper.getLastName());

        locationListener();
        askLocationPermission();

    }

    private void getDashBoardDetails() {

        DialogUtils.showLoader(requireContext());
        commonViewModel.dashBoardDetails(requireContext()).observe(this, new Observer<DashboardResponse>() {
            @Override
            public void onChanged(DashboardResponse profileResponse) {
                DialogUtils.dismissLoader();
                if (profileResponse.getError()) {
                    Utils.showSnack(binding.parentView, profileResponse.getMessage());
                } else {
                    setData(profileResponse.getData().getProfile(), profileResponse.getData().getCurrentAssignment(), profileResponse.getData().getNewCar());
                }
            }
        });
    }

    private void setData(DashboardData profile, ArrayList<CurrentAssignment> currentAssignment, NewCar newCar) {

        if (profile == null)
            return;

        if (currentAssignment.size() != 0) {
            ((DashboardActivity) requireActivity()).currentAssignmentRouteId = currentAssignment.get(0).getId();
            currentAssignmentRouteId = currentAssignment.get(0).getId();
            sharedHelper.setcurrentAssignmentRouteId(currentAssignment.get(0).getId());
        } else {
            ((DashboardActivity) requireActivity()).currentAssignmentRouteId = "";
            currentAssignmentRouteId = "";
            sharedHelper.setcurrentAssignmentRouteId("");
        }


        setOnlineStatus(profile.getDriverActive());

        Log.d("cxcxv",""+profile.getFloatingCash());
        binding.floatingCash.setText("SAR "+profile.getFloatingCash());

        if (profile.getCarID() != null && !profile.getCarID().equalsIgnoreCase("") && profile.isAccepted() == 1) {
            binding.carNumber.setText(profile.getCarID());
            binding.textView7.setVisibility(View.VISIBLE);
            binding.imageView7.setVisibility(View.VISIBLE);
            binding.carNumber.setVisibility(View.VISIBLE);
            binding.imageView10.setVisibility(View.VISIBLE);
            binding.assignedCardView.setVisibility(View.VISIBLE);
        } else {
            binding.textView7.setVisibility(View.GONE);
            binding.imageView7.setVisibility(View.GONE);
            binding.carNumber.setVisibility(View.GONE);
            binding.imageView10.setVisibility(View.GONE);
            binding.assignedCardView.setVisibility(View.GONE);
        }

        if (newCar != null && newCar.getCar().size() != 0) {

            binding.carNumberNew.setText(newCar.getCar().get(0).getCarID());

            binding.textView8.setVisibility(View.VISIBLE);
            binding.view3.setVisibility(View.VISIBLE);
            binding.imageView17.setVisibility(View.VISIBLE);
            binding.textView17.setVisibility(View.VISIBLE);
            binding.carNumberNew.setVisibility(View.VISIBLE);
            binding.newCarGo.setVisibility(View.VISIBLE);

        } else {

            binding.textView8.setVisibility(View.GONE);
            binding.view3.setVisibility(View.GONE);
            binding.imageView17.setVisibility(View.GONE);
            binding.textView17.setVisibility(View.GONE);
            binding.carNumberNew.setVisibility(View.GONE);
            binding.newCarGo.setVisibility(View.GONE);
        }


        if (currentAssignment.size() != 0) {
            binding.cardView2.setVisibility(View.VISIBLE);
        } else {
            binding.cardView2.setVisibility(View.GONE);
        }
        if (profile.getExpirationDate() != null) {
            if (currentAssignment.size() != 0){
                binding.assignedDate.setText(
                        new SpannableStringBuilder().append(Utils.getColoredAndBoldString(requireContext(), getString(R.string.date), R.color.colorPrimary))
                                .append(" ")
                                .append(Utils.getColoredAndBoldString(requireContext(), Utils.getConvertedTime(currentAssignment.get(0).getAssignDate(), "dd MMM,yyyy"), R.color.edit_text_color)));

                binding.assignedTime.setText(
                        new SpannableStringBuilder().append(Utils.getColoredAndBoldString(requireContext(), getString(R.string.time), R.color.colorPrimary))
                                .append(" ")
                                .append(Utils.getColoredAndBoldString(requireContext(), Utils.getConvertedTime(currentAssignment.get(0).getAssignDate(), "hh:mm a"), R.color.edit_text_color)));
                
            }

            if (Utils.getBonusTime(profile.getExpirationDate()).equalsIgnoreCase("")) {
                binding.assignmentBonus.setText(R.string.bonus_expired);

            } else {
                binding.assignmentBonus.setText(
                        new SpannableStringBuilder().append(Utils.getColoredAndBoldString(requireContext(), getString(R.string.complete_assignment_in), R.color.edit_text_color))
                                .append(" ")
                                .append(Utils.getColoredAndBoldString(requireContext(), Utils.getBonusTime(profile.getExpirationDate()), R.color.colorPrimary))
                                .append(" ")
                                .append(Utils.getColoredAndBoldString(requireContext(), getString(R.string.for_bonus), R.color.edit_text_color)));

            }


        }


    }

    private void setValues() {

        binding.name.setText(getString(R.string.hi)+" "+sharedHelper.getFirstName() + " " + sharedHelper.getLastName());
    }

    private void setOnlineStatus(String val) {
        if (val.equalsIgnoreCase("1")) {
            binding.imageOn.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.rdt_checked));
            binding.imageOff.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.rdt_unchecked));
        } else {
            binding.imageOff.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.rdt_checked));
            binding.imageOn.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.rdt_unchecked));
        }
    }

}
