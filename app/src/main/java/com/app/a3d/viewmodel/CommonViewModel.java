package com.app.a3d.viewmodel;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.a3d.model.AssignmentResponse;
import com.app.a3d.model.CommonResponse;
import com.app.a3d.model.DashboardResponse;
import com.app.a3d.model.GetRouteResponse;
import com.app.a3d.model.MaintenanceListResponse;
import com.app.a3d.model.NotificationResponseModel;
import com.app.a3d.model.ProfileResponse;
import com.app.a3d.model.ViewRouteResponse;
import com.app.a3d.network.URLHelper;
import com.app.a3d.repository.CommonRepository;
import com.app.a3d.utils.SharedHelper;
import com.app.a3d.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class CommonViewModel extends AndroidViewModel {

    CommonRepository repository = new CommonRepository();
    public MutableLiveData<String> time = new MutableLiveData<>();


    public CommonViewModel(@NonNull Application application) {
        super(application);
    }


    public LiveData<DashboardResponse> dashBoardDetails(Context context) {


        return repository.dashBoardDetails(Utils.getInputs(context, null, URLHelper.DASHBOARD));
    }


    public LiveData<ProfileResponse> getProfileDetails(Context context) {
        return repository.getProfileDetails(Utils.getInputs(context, null, URLHelper.GETPROFILE));
    }

    public LiveData<CommonResponse> uploadDocument(Context context, String message, String currentImageType) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("type", currentImageType);
            jsonObject.put("document", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.uploadDocument(Utils.getInputs(context, jsonObject, URLHelper.UPLOADDOCUMENT));
    }

    public LiveData<CommonResponse> updateProfile(Context context, String trim, String trim1, String trim2, String mobileNumber, boolean checked, String trim3) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("firstName", trim);
            jsonObject.put("lastName", trim1);
            jsonObject.put("email", trim2);
            jsonObject.put("mobileNumber", mobileNumber);
            if (checked) {
                jsonObject.put("gender", "MALE");
            } else {
                jsonObject.put("gender", "FEMALE");
            }

            jsonObject.put("dob", trim3);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.updateProfile(Utils.getInputs(context, jsonObject, URLHelper.UPDATEPROFILE));
    }

    public LiveData<CommonResponse> updateStatus(Context context, int i) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("isDeleteDriver", i);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.updateStatus(Utils.getInputs(context, jsonObject, URLHelper.UPDATEDRIVERSTATUS));

    }

    public MutableLiveData<MaintenanceListResponse> getMaintenanceList(Context context) {
        return repository.getMaintenanceList(Utils.getInputs(context, null, URLHelper.MAINTANANCELIST));
    }

    public MutableLiveData<CommonResponse> addMaintenance(Context context, String carID,
                                                          String selectedMaintenanceId, String trim, String trim1, String trim2) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("carId", carID);
            jsonObject.put("maintenanceId", selectedMaintenanceId);
            jsonObject.put("maintenanceAmount", trim);
            jsonObject.put("currentMileage", trim1);
            jsonObject.put("message", trim2);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.addMaintenance(Utils.getInputs(context, jsonObject, URLHelper.ADDMAINTANANCE));

    }

    public MutableLiveData<CommonResponse> returnCar(Context context, String mileage, String ucarId, String newDamages) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("carId", ucarId);
            jsonObject.put("carMileage", mileage);
            jsonObject.put("damages", newDamages);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.returnCar(Utils.getInputs(context, jsonObject, URLHelper.RETURNCAR));
    }

    public MutableLiveData<GetRouteResponse> getRoutes(Context context, String routeId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("routeId", routeId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getRoutes(Utils.getInputs(context, jsonObject, URLHelper.GETROUTES));

    }

    public MutableLiveData<ViewRouteResponse> getViewRoutes(Context context, String routeId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("routeId", routeId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getViewRoutes(Utils.getInputs(context, jsonObject, URLHelper.GETROUTES));

    }

    public MutableLiveData<CommonResponse> updateProductStatus(Context context, String position) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", position);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.updateProductStatus(Utils.getInputs(context, jsonObject, URLHelper.UPDATEORDERSTATUS));

    }

    public MutableLiveData<CommonResponse> uploadRecipt(Context context, String u_id, String storeI, String message, String routeId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("u_id", u_id);
            jsonObject.put("routeId", routeId);
            jsonObject.put("storeId", storeI);
            jsonObject.put("receipt", message);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.uploadRecipt(Utils.getInputs(context, jsonObject, URLHelper.UPLOADRECIPT));


    }

    public MutableLiveData<AssignmentResponse> getAssignmetnList(Context context) {


        return repository.getAssignmetnList(Utils.getInputs(context, null, URLHelper.GETASSIGNMENT));


    }

    public LiveData<CommonResponse> moveToLast(Context context, String id, String routeId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", id);
            jsonObject.put("routeId", routeId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.moveToLast(Utils.getInputs(context, jsonObject, URLHelper.MOVETOLAST));

    }

    public LiveData<CommonResponse> makeDelivery(Context context,Double amount, String userid, String routeId, String orderId, String notes, String yesOrNo) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("orderId", orderId);
            jsonObject.put("routeId", routeId);
            jsonObject.put("userId", userid);
            jsonObject.put("amount", amount);
            jsonObject.put("notes", notes);
            jsonObject.put("findLocation", yesOrNo);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.makeDelivery(Utils.getInputs(context, jsonObject, URLHelper.MAKEDELIVERY));

    }

    public LiveData<CommonResponse> finalDelivery(Context context, String routeId) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("routeId", routeId);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.finalDelivery(Utils.getInputs(context, jsonObject, URLHelper.FINALDELIVERYCALL));

    }

    public MutableLiveData<CommonResponse> acceptCar(Context context) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("isAccepted", 1);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.acceptCar(Utils.getInputs(context, jsonObject, URLHelper.ACCEPTCAR));

    }

    public void updateLatLng(Context context, String currentLat, String currentLng) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("latitude", currentLat);
            jsonObject.put("longitude", currentLng);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        repository.updateLatLng(Utils.getInputs(context, jsonObject, URLHelper.UPDATELATLNG));

    }

    public void updateDeviceToken(Context context) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("fcmToken", new SharedHelper(context).getFirebaseToken());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        repository.updateDeviceToken(Utils.getInputs(context, jsonObject, URLHelper.UPDATEDEVICETOKEN));

    }

    public MutableLiveData<NotificationResponseModel> getNotificationList(Context context) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("pageNumber", 1);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return repository.getNotificationList(Utils.getInputs(context, jsonObject, URLHelper.DRIVERNOTIFICATION));


    }
}
